{{-- Section FAQ List --}}
<div class="section faq-list">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3><ion-icon name="help-circle-outline" class="icon mr-2"></ion-icon>Daftar FAQ</h3>
            </div>

            <div class="col-12 mt-4">
                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer">Bagaimana cara memesan tiket bus Tami Jaya?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Anda dapat pergi garasi bus Tami Jaya atau menghubungi petugas resmi Tami Jaya untuk memesan tiket.
                                <br><br>
                                Hati-hati dengan marketing atau petugas palsu yang mengatasnamakan Staff Tami Jaya. Petugas resmi hanya yang tertera pada official akun kami <a class="text-dark" target="_blank" href="instagram.com/tamijaya_transport">@tamijaya_transport</a> atau yang sudah kami informasikan pada menu kontak di website ini.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer-2" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer-2">Fasilitas apa saja yang didapatkan?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Fasilitas yang tersedia tergantung armada. Secara umum, fasilitas yang ada yaitu bus AC, audio video, karaoke, bagasi luas, dan reclining seat.
                                <br><br>
                                Informasi lebih lengkap dapat melihat brosur atau pada menu Armada.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer-3" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer-3">Berapa harga tiket bus Tami Jaya?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Harga tiket bus bervariasi, tergantung dari titik keberangkatan dan kedatangan. Anda dapat menghubungi nomor admin kami atau sosial media kami.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer-4" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer-4">Pembayaran dilakukan melalui apa?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Untuk pembayaran DP & Pelunasan dapat ditransfer langsung ke Rekening:
                                <br>
                                BCA - 037 567 0670
                                <br>
                                Mandiri - 13700 567 0670 4
                                <br><br>
                                a/n PT ANUGERAH KARYA UTAMI GEMILANG
                                <br><br>
                                Khusus pembelian tiket bus malam, mohon pastikan rencana keberangkatan anda sudah benar dan sesuai.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer-5" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer-5">Titik keberangkatan dan kedatangan untuk Bus Reguler Jogja-Bali atau Bali-Jogja dimana saja?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Untuk informasi lebih lengkap dapat dilihat pada menu Rute Bus Tami Jaya atau jika anda merasa informasi yang kurang jelas, dapat menghubungi petugas kami.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="faq-drawer">
                    <input class="faq-drawer__trigger" id="faq-drawer-6" type="checkbox" />
                    <label class="faq-drawer__title" for="faq-drawer-6">Bagaimana jika ingin melakukan reschedule?</label>
                    <div class="faq-drawer__content-wrapper">
                        <div class="faq-drawer__content pt-4">
                            <p>
                                Reschedule dapat dilakukan dengan menghubungi petugas terkait dengan ketentuan dilakukan PALING LAMBAT 1 hari sebelum keberangkatan dan menyesuaikan ketersediaan seat.
                            </p>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>