{{-- Section Bus Route --}}
<section class="section bus-route py-0">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4 mb-md-2">
                <h3>
                    <ion-icon name="map-outline" class="mr-1"></ion-icon> 
                    Rute Bus Tamijaya
                </h3>
            </div>

            <div class="col-12 mb-4 mb-md-2">
                <h4 class="text-uppercase mb-3">Jogja - Bali</h4>
            </div>

            <div class="col-12 px-0">
                <div class="owl-carousel owl-theme" style="z-index: 0" id="owl-carousel">
                    @php
                        $images = ['7.png', '8.png', '9.png', '10.png', '11.png', '12.png', '13.png'];
                    @endphp

                    @foreach ($images as $image)
                        <div class="item">
                            <div class="card--news-banner">
                                @php
                                    $img_src = 'images/' . $image;
                                @endphp
                                <img src="{{ asset($img_src) }}" class="w-100 img-fluid" alt="News Banner">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 mb-4 mb-md-2">
                <h4 class="text-uppercase mb-3">Bali - Jogja</h4>
            </div>

            <div class="col-12 px-0">
                <div class="owl-carousel owl-theme" style="z-index: 0" id="owl-carousel-2">
                    @php
                        $images = ['14.png', '15.png', '16.png', '17.png'];
                    @endphp

                    @foreach ($images as $image)
                        <div class="item">
                            <div class="card--news-banner">
                                @php
                                    $img_src = 'images/' . $image;
                                @endphp
                                <img src="{{ asset($img_src) }}" class="w-100 img-fluid" alt="News Banner">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-12 mb-4 mb-md-2 mt-5">
                <h4 class="text-center">Layanan Aduan dan Barang Ketinggalan</h4>
                <hr>
                <p class="mb-3">
                    Kami memahami bahwa kehilangan barang di perjalanan bisa menjadi pengalaman yang menyulitkan. Di PT Anugerah Karya Utami Gemilang ( Tami Jaya ) kami berkomitmen untuk membantu Anda menyelesaikan masalah tersebut dengan cepat dan efisien.
                </p>
                <p style="font-weight: bold">Prosedur Pengaduan :</p>
                <p>
                    <span style="font-weight: bold">1. Hubungi Kami:</span> Jika Anda kehilangan barang di dalam bus kami, segera hubungi pusat layanan kami melalui kontak yang kami sertakan. Berikan informasi yang jelas dan detail tentang barang yang hilang serta rincian perjalanan Anda.
                    <br>
                    <span style="font-weight: bold">2. Identifikasi Barang:</span> Untuk memudahkan kami dalam pencarian, mohon berikan deskripsi yang lengkap tentang barang yang hilang, termasuk warna, merek, dan ciri-ciri khusus lainnya.
                    <br>
                    <span style="font-weight: bold">3. Proses Penelusuran:</span> Tim kami akan segera memulai proses penelusuran untuk mencari barang yang hilang. Kami akan berkoordinasi dengan staf kami di lapangan dan memberi Anda pembaruan secara berkala tentang kemajuan pencarian.
                    <br>
                    <span style="font-weight: bold">4. Pengembalian Barang:</span> Jika barang Anda berhasil ditemukan, kami akan menghubungi Anda untuk mengatur pengembalian barang tersebut. Anda dapat memilih untuk mengambil barang langsung di kantor kami atau kami akan mengirimkannya ke alamat yang Anda berikan.
                </p>
                <br>
                <p style="font-weight: bold">Kontak Kami:</p>
                <p>
                    Telepon: <a target="_blank" style="color: black" href="https://wa.me/62811250147">+62 811-250-147</a> <a target="_blank" style="color: black" href="https://www.instagram.com/tamijaya_transport/">Instagram:tamijaya_transport</a> Jam Layanan: Senin-Sabtu, 08.00-16.00
                </p>
                <br>
                <p>
                    Layanan aduan dan barang ketinggalan kami hadir untuk memberikan kenyamanan tambahan bagi para penumpang kami. Kami berusaha keras untuk memastikan bahwa setiap masalah yang Anda hadapi ditangani dengan profesionalisme dan kehati-hatian yang tinggi.
                    <br>
                    Terima kasih atas kerja sama Anda.
                </p>
            </div>
            {{-- <div class="col-12 mb-4 mb-md-2">
                <h4 class="text-uppercase mb-3">Suitess Class</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-routes">
                        <thead>
                            <tr>
                                <th scope="col">RUTE</th>
                                <th scope="col">SENIN</th>
                                <th scope="col">SELASA</th>
                                <th scope="col">RABU</th>
                                <th scope="col">KAMIS</th>
                                <th scope="col">JUMAT</th>
                                <th scope="col">SABTU</th>
                                <th scope="col">MINGGU</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">JOGJA - DENPASAR</th>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                            </tr>
                            <tr>
                                <th scope="row">DENPASAR - JOGJA</th>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> --}}

            {{-- <div class="col-12">
                <h4 class="text-uppercase mb-3">Executive Class</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-routes">
                        <thead>
                            <tr>
                                <th scope="col">RUTE</th>
                                <th scope="col">SENIN</th>
                                <th scope="col">SELASA</th>
                                <th scope="col">RABU</th>
                                <th scope="col">KAMIS</th>
                                <th scope="col">JUMAT</th>
                                <th scope="col">SABTU</th>
                                <th scope="col">MINGGU</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">JOGJA - DENPASAR</th>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                            </tr>
                            <tr>
                                <th scope="row">DENPASAR - JOGJA</th>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="checkmark-circle-outline"></ion-icon></td>
                                <td><ion-icon name="remove-outline"></ion-icon></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> --}}
        </div>
    </div>
</section>