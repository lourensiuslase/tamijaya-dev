@include('components.modals.modal-login-register')

<script>
    const loginCustomer = () => {
    var customer_email = $("#customer_email").val();
    var customer_password = $("#customer_password").val();
    if (customer_email == "" || customer_password == "") {
        console.log("Silahkan lengkapi email dan password anda");
        return;
    }
    $.ajax({
        type: "POST",
        url: "/customer/login",
        data: {
            customer_email: customer_email,
            customer_password: customer_password
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            console.log(response.status);
            if (response.status == '400') {
                console.log(response.message);
                return;
            }
            console.log(response.message);
            $("#customer_email").val('');
            $("#customer_password").val('');

            localStorage.setItem("customerData", btoa(JSON.stringify(response.data)));
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    });
}

</script>