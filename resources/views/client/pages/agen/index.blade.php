<title>Tami Jaya Transport - Travel</title>

@extends('layouts.app-web')

@section('content')

    <x-agen.section-agen-header />
    <x-travel.section-travel-info :offices="$offices" />

@endsection

@push('page-scripts')
    <script>
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            }
        });

        const checkPickPoint = () => {
            var area = $("#area").val();
            $.ajax({
                url: `/admin/master-data/agen-pool/get-agen-pool/${area}`,
                method: "get",
                dataType: "json",
                success: function(response) {
                    var agenPool = response.data;
                    var html = ``;
                    agenPool.forEach(ap => {
                        html += `<div class="col-12 col-md-6 col-lg-4">
                                    <div class="card--pickup-point">
                                        <h4 class="mt-2">${ap.name}</h4>
                                        <a href="${ap.link_maps}" target="_blank">
                                            <h6 style="color: #C61010" class="fw-bold text-uppercase">Lokasi</h6>
                                            <span>${ap.lokasi}</span>
                                        </a>
                                        <a href="https://wa.me/${ap.number_phone}" target="_blank">
                                            <h6 style="color: #C61010" class="fw-bold text-uppercase">No. Handphone</h6>
                                            <span>+${ap.number_phone}</span>
                                        </a>
                                    </div>
                                </div>`;
                    })
                    $("#result-agen-pool").html(html)

                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
    </script>
@endpush
