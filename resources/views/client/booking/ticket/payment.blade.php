<title>Tami Jaya Transport - Armada</title>

@extends('layouts.app-web')

@section('content')
    <style>
        .bx--copy-alt {
            display: inline-block;
            width: 1em;
            height: 1em;
            --svg: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'%3E%3Cpath fill='%23000' d='M20 2H10c-1.103 0-2 .897-2 2v4H4c-1.103 0-2 .897-2 2v10c0 1.103.897 2 2 2h10c1.103 0 2-.897 2-2v-4h4c1.103 0 2-.897 2-2V4c0-1.103-.897-2-2-2M4 20V10h10l.002 10zm16-6h-4v-4c0-1.103-.897-2-2-2h-4V4h10z'/%3E%3Cpath fill='%23000' d='M6 12h6v2H6zm0 4h6v2H6z'/%3E%3C/svg%3E");
            background-color: currentColor;
            -webkit-mask-image: var(--svg);
            mask-image: var(--svg);
            -webkit-mask-repeat: no-repeat;
            mask-repeat: no-repeat;
            -webkit-mask-size: 100% 100%;
            mask-size: 100% 100%;
        }
    </style>

    <section class="section armada-header">
        <div class="container">
            <form action="{{ route('transfer') }}" id="form-transaction-reguler" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="booking_transactions_pick_point" value="{{ $data['pick_point_id'] }}">
            <input type="hidden" name="booking_transactions_arrival_point" value="{{ $data['arrival_point_id'] }}">
            <input type="hidden" name="booking_transactions_travel_type_name" value="{{ $data['travel_type_name'] }}">
            <input type="hidden" name="booking_transactions_travel_price" value="{{ $data['travel_price'] }}">
            <input type="hidden" name="booking_transactions_schedule_id" value="{{ $data['id_schedule'] }}">
            <input type="hidden" name="booking_transactions_total_costs" value="{{ $data['travel_detail_total_cost'] }}">
            <input type="hidden" name="booking_transactions_total_seats" value="{{ $data['travel_passenger'] }}">
            <input type="hidden" name="booking_transactions_reschedule_date" value="{{ $data['reschedule_date'] }}">
            <input type="hidden" name="travel_selected_seat" value="{{ $data['travel_selected_seat'] }}">

            {{-- penumpang --}}
            <input type="hidden" name="name" value="{{ $data["name"] }}">
            <input type="hidden" name="nik" value="{{ $data["nik"] }}">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-3">Pembayaran Tiket Bus Tami Jaya</h3>
                    <div class="card--top-armada">
                        {{-- <div class="row"> --}}
                            
                            {{-- START PAYMENT DETAIL --}}
                            <h6>Payment Method: <span style="color:red;">*</span></h6>
                            <div class="row">
                                    <div class="col-4 col-md-4 " style="border-right:1px solid #cecece">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <input type="radio" name="booking_transactions_payment" onchange="choosePayment()" id="payment_cash" checked>
                                                    <label for="payment_cash">CASH</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="radio">
                                                    <input type="radio" name="booking_transactions_payment" value="TRANSFER" onchange="choosePayment()" id="payment_transfer">
                                                    <label for="payment_transfer">TRANSFER</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="row-bank-transfer" style="display: none">
                                            <div class="col-md-12">
                                                <label>Bank Transfer: <span style="color:red;">*</span></label>
                                                <div class="form-group">
                                                    <select id="id_bank_transfer" onchange="chooseBank()" name="booking_transactions_id_payment"
                                                        class="form-control">
                                                        <option value="">Choose Bank</option>
                                                        @foreach ($banks as $bank)
                                                            <option value="{{ $bank->id }}">{{ $bank->bank_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <h5>Keterangan Payment Method</h5> --}}
                                    <div class="col-6 col-md-6">
                                        <h6 style="font-weight: 500">Keterangan Payment Method: </h6>
                                        <div id="payment-method-information">

                                            {{-- PAYMENT CASH --}}
                                            <div class="" id="payment-description-cash">
                                                <label style="font-weight: 500">Total Pembayaran dengan cash:</label>
                                                <div class="d-flex">
                                                    <h6 class="font-weight-bold title-transfer-amount">Rp. {{ number_format($total_price) }}</h6>
                                                </div>
                                            </div>

                                            {{-- PAYMENT TRANSFER --}}
                                            <div class="" id="payment-description-transfer" style="display:none">
                                                <div class=" d-flex justify-content-between">
                                                    <h6 style="font-weight: 500; font-size: 24px" id="title-transfer-payment">Payment Transfer </h6>
                                                    <img id="image-transfer-payment" style="width: 20%" src="" alt="">
                                                </div>
                                                <div class="card-content collapse show px-0">
                                                    <div class="card-body px-0">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <label>Nomor Rekening:</label>
                                                                <h6 class="font-weight-bold" id="title-transfer-account"></h6>
                                                            </div>
                                                            <div class="col-md-4">
                                                                {{-- <h6 for="" class="font-weight-bold pointer mt-1 text-primary">Salin <span class="bx--copy-alt"></span></h6> --}}
                                                            </div>
                                                            <div class="col-md-8">
                                                                <label>Total Pembayaran:</label>
                                                                <div class="d-flex">
                                                                    <h6 class="font-weight-bold title-transfer-amount" >Rp. {{ number_format($total_price) }}</h6><i
                                                                        class="bx bx-copy-alt text-primary pointer"></i>
                                                                </div>
                                                            </div>
                                                            {{-- <div class="col-md-4">
                                                                <h6 for="" class="font-weight-bold pointer mt-1 text-primary">Lihat Detail</h6>
                                                            </div> --}}
                                                            <hr>
                                                            <div class="col-md-12">
                                                                <h6 style="font-weight: 500">Lampirkan Bukti Transfer:</h6>
                                                                <div class="form-group">
                                                                    <input type="file" 
                                                                            name="booking_transactions_transactions_pic" 
                                                                            class="form-control"
                                                                            accept="image/*" 
                                                                            id="imgInp" />
                                                                    <img id="blah" class="w-100">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group mt-2">
                                                                    <div id="preview" class="text-center"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="w-100">
                                                <h6 style="font-weight: 500">Down Payment:</h6>
                                                <div class="input-group mb-1">
                                                    <div class="input-group-prepend">
                                                    <select id="type_down_payment"  name="booking_transactions_type_down_payment" class="form-control" style="width:70px;border-color:#DFE3E7 !important;" onchange="changeTypeDP(this)" >
                                                            <option value="NOMINAL">Rp</option>
                                                            <option value="PERCENT">%</option>
                                                    </select>
                                                    </div>
                                                    <input onchange="checkValidityTotalDP(this)" 
                                                            type="text" 
                                                            class="form-control input-number" 
                                                            id="total_down_payment" 
                                                            name="booking_transactions_total_down_payment">
                                                </div>
                                            </div>
                                            <div class="w-100">
                                                <button type="submit" class="btn btn-danger w-100" >Pesan Tiket Sekarang</button>
                                            </div>

                                        </div>
                                    </div>
                            </div>
                            {{-- END PAYMENT DETAIL --}}
                          
                        {{-- </div> --}}
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="section armada-list">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel owl-theme" id="owl-carousel-3">

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@push('page-scripts')
    <script src="{{ asset('script/admin/transaction/dashboard.js') }}"></script>
@endpush

