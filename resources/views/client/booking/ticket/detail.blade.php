<title>Tami Jaya Transport - Armada</title>

@extends('layouts.app-web')

@section('content')
    <style>

        ul {
            padding: 0;
            margin: 0;
        }
        li {
            list-style: none;
        }
        figure {
            margin: 0;
        }

        .main {
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            background: #ffffff;
        }
        .app {
            width: 375px;
            height: 667px;
            border: 1px solid #efefef;
            position: relative;
            background-color: white;
            border-radius: 10px;
            overflow: hidden;
        }

        /* ========== Home ========== */
        .screen-home__location,
        .screen-home__date {
            margin-bottom: 30px;
        }
        .screen-home {
            width: 100%;
            position: absolute;
            z-index: 1;
        }
        .screen-home__form-wrap {
            padding: 0 1rem;
        }
        .screen-home__form {
            padding: 40px 0 0 0;
        }
        .screen-home__location .lable {
            display: flex;
            align-items: center;
        }
        .lable {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }
        .lable .icon {
            margin: 0 10px 0 0;
        }
        .lable .text {
            font-family: roboto;
        }
        .inside-wrap {
            background-color: #f6f5f5;
            /*padding: 10px 0px;*/
            position: relative;
            border-radius: 5px;
        }
        .inside-lable {
            font-size: 0.7rem;
            padding: 0px 0 5px 0;
            display: inline-block;
        }
        .input {
            width: 100%;
            border: 0;
            padding: 8px 0;
            font-size: 1.4rem;
            background: none;
            outline: none;
            color: #000000;
        }
        .from {
            border-bottom: 2px solid #070707;
        }
        .from, .to {
            padding: 8px 15px;
        }
        .rotate-btn {
            position: absolute;
            right: 20px;
            height: 100%;
            display: flex;
            align-items: center;
        }
        .rotate-btn figure {
            margin: 0;
            width: 40px;
            height: 40px;
            background-color: #ffffff;
            border-radius: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 2px solid #66a1f3;
        }
        .screen-home__date .inside-wrap {
            display: flex;
            padding: 4px 15px;
        }
        .onward {
            width: 50%;
            position: relative;
        }
        .return {
            width: calc(50% - 15px);
            padding-left: 15px;
        }
        .onward:before {
            content: '';
            position: absolute;
            width: 1px;
            height: 35px;
            background-color: #ffffff;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;
        }
        .onward,
        .return {
            display: flex;
        }
        .onward .input,
        .return .input {
            width: 37px;
        }
        .mon-day {
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            font-size: 0.7rem;
            padding-left: 13px;
            color: #000000;
        }
        .mon-day:before {
            content: "";
            position: absolute;
            left: 0;
            width: 1px;
            height: 20px;
            background-color: #ffffff;
            top: 0;
            bottom: 0;
            margin: auto 0;
        }
        .month {
            padding-top: 4px;
        }
        .day {
            padding-bottom: 4px;
        }
        .inside-lable-wrap {
            display: flex;
        }
        .inside-lable-wrap .inside-lable-col {
            width: 50%;
        }
        .screen-bottom {
            position: absolute;
            bottom: 0;
            width: 100%;
            padding: 15px 0;
        }
        .screen-bottom ul {
            display: flex;
            justify-content: space-between;
            padding: 0 15px;
        }
        .screen-home__submit-wrap .screen-home__bus-page {
            background-color: #ffffff;
            width: 46px;
            height: 46px;
            position: relative;
            border-radius: 100px;
            display: flex;
            align-items: center;
            justify-content: center;
            outline: none;
            padding: 0;
            border: 3px solid #ffffff;
        }
        .screen-home__submit-wrap .screen-home__bus-page figure {
            height: 26px;
            cursor: pointer;
        }
        .screen-home__submit-wrap {
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
        }
        .line {
            position: absolute;
            width: 100%;
            height: 1px;
            background-color: #ffffff;
            z-index: -1;
        }


        .screen-home__recent-search {
            margin-top: 20px;
        }
        .screen-home__rs-col {
            display: flex;
            justify-content: space-between;
            padding: 0.5rem 1rem;
            border: 1px solid #c7deff;
            border-radius: 4px;
            font-size: 0.8rem;
            margin-bottom: 1rem;
        }
        .screen-homers-from-to {
            display: flex;
            align-items: center;
        }
        .screen-home__rs-arrow {
            display: inline-block;
            width: 20px;
            height: 1px;
            background-color: #000000;
            margin: 0 10px;
            position: relative;
        }
        .screen-home__rs-arrow:before,
        .screen-home__rs-arrow:after {
            content: "";
            width: 6px;
            height: 1px;
            background-color: #000000;
            position: absolute;
            right: 0;
        }
        .screen-home__rs-arrow:before {
            transform: rotate(45deg);
            top: -2px;
        }
        .screen-home__rs-arrow:after {
            transform: rotate(-45deg);
            top: 2px;
        }


        /* ========== Bus ========== */
        .screen-bus {
            opacity: 0;
        }
        .screen-bus__location-filter-row {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: center;
            background-color: #ffffff;
        }
        .screen-bus__location {
            padding: 1.3rem 15px;
            color: #ffffff;
        }
        .screen-bus__filter {
            padding-right: 1rem;
        }
        .screen-bus__location-row {
            margin-bottom: 1rem;
            display: flex;
            align-items: center;
            font-size: 0.9rem;
        }
        .screen-bus__date-row {
            font-size: 0.7rem;
        }
        .screen-bus__center-arrow{
            display: inline-block;
            width: 18px;
            height: 18px;
            background-color: #ffffff;
            border-radius: 100px;
            margin: 0 1.2rem;
        }
        .screen-bus__travels-wrap {
            padding: 1rem 1rem 0 1rem;
        }
        .screen-bus__travels-col {
            box-shadow: 0px 1px 4px rgba(170, 170, 170, 0.25);
            border-radius: 3px;
            padding: 12px;
            border: 1px solid whitesmoke;
            margin-bottom: 1rem;
            opacity: 0;
            transform: translateY(5px);
        }
        .screen-bus__name-time-seat {
            display: flex;
            justify-content: space-between;
        }
        .screen-bus__name-wrap,
        .screen-bus__time-wrap,
        .screen-bus__seat-wrap {
            width: 33.333%;
        }
        .screen-bus__name-wrap {
            display: flex;
            flex-direction: column;
            font-size: 0.7rem;
        }
        .screen-bus__name {
            margin-bottom: 0.3rem;
        }
        .screen-bus__type,
        .screen-bus__hrs span {
            font-size: 0.6rem;
            color: #cacaca;
        }
        .screen-bus__seat-wrap {
            text-align: right;
            font-size: 0.7rem;
        }
        .screen-bus__count {
            font-size: 0.9rem;
            color: #81e276;
        }
        .screen-bus__time-wrap {
            font-size: 0.7rem;
            display: flex;
            flex-direction: column;
        }
        .screen-bus__time {
            display: flex;
            margin-bottom: 0.3rem;
        }
        .screen-bus__time-arrow-wrap {
            margin: 0 0.7rem;
        }
        .screen-bus__time-arrow {
            display: inline-block;
            width: 15px;
            height: 1px;
            background-color: red;
            position: relative;
        }
        .screen-bus__time-arrow:after,
        .screen-bus__time-arrow:before {
            content: "";
            width: 5px;
            height: 1px;
            background-color: red;
            position: absolute;
            right: 0;
        }
        .screen-bus__time-arrow:after {
            transform: rotate(-45deg);
            top: 2px;
        }
        .screen-bus__time-arrow:before {
            transform: rotate(45deg);
            top: -2px;
        }
        .screen-bus__rating-price-row {
            display: flex;
            justify-content: space-between;
        }
        .screen-bus__rating-price {
            margin-top: 0.5rem;
        }
        .screen-bus__rating-row {
            display: flex;
        }
        .screen-bus__rating-row li {
            margin-right: 5px;
        }
        .screen-bus__rating-row li:last-child {
            margin-right: 0;
        }
        .screen-home__inside-wave {
            width: 0%;
            height: 0%;
            position: absolute;
            border-radius: 100px;
            background-color: #ffffff7d;
            cursor: pointer;
        }
    </style>
    <style>
        .nav-tabs-bus .nav-item.active .nav-link {
            background-color: #1aff00; /* Ganti #ff0000 dengan kode warna yang Anda inginkan */
            color: #ffffff; /* Ganti #ffffff dengan kode warna teks yang Anda inginkan */
        }
    
        .nav-tabs-bus .nav-item .nav-link {
            background-color: #00ff00; /* Ganti #00ff00 dengan kode warna yang Anda inginkan */
            color: #000000; /* Ganti #000000 dengan kode warna teks yang Anda inginkan */
        }
    
        .bus-info h5 {
            background-color: #ffffff; /* Ganti #0000ff dengan kode warna yang Anda inginkan */
            color: #ffffff; /* Ganti #ffffff dengan kode warna teks yang Anda inginkan */
        }
    
        ul.seats .seat input[type="checkbox"]:checked + label::before {
            background-color: #ff00ff; /* Ganti #ff00ff dengan kode warna yang Anda inginkan */
        }
    
    
    </style>
    @foreach ($detailBus as $bus)
        <section class="section armada-header">
            <div class="container">
                <form action="{{ route('payment') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="destination" value="{{ $bus->destination }}">
                <input type="hidden" id="id_schedule" name="id_schedule" value="{{ $bus->id }}">
                <input type="hidden" name="pick_point_origin" value="{{ $bus->pick_point_origin }}">
                <input type="hidden" name="pick_point_name" value="{{ $bus->pick_point_name }}">
                <input type="hidden" name="reschedule_date" value="{{ $bus->date_departure }}">
                {{-- START TRAVEL DETAIL --}}
                <div class="row">
                    <div class="col-12">
                        <h3 class="mb-3">Pemesanan Tiket Bus Tami Jaya</h3>
                        <div class="card--top-armada">
                            <div class="row">

                                {{-- start detail bus --}}
                                    <div id="seat-selection">
                                        <h5 style="margin-left: 175px" class="font-weight-bold">Pick Seat</h5>
                                        <div class="row">
                                            <link href="{{ asset('css/animate-seat.css') }}" rel="stylesheet">
                                            <div class="col-md-6" id="display-seat">
                                            
                                            {{-- start display seat "SUITESS" --}}
                                            @if ($bus->armada_type === 'SUITESS')
                                                <ul class="nav nav-tabs-bus" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Ground</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Top</a>
                                                    </li>
                                                </ul>
                                            @endif
                                            <div class="tab-content">
                                                @if ($bus->armada_type === 'SUITESS')
                                                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                                        
                                                        <h4 class="text-center">Ground Floor</h4>
                                                        <div class="bus">
                                                            <div class="bus-info">
                                                                <h5 class="text-center">Suite</h5>
                                                            </div>
                                                
                                                            <div class="bus-door"></div>
                                                
                                                            <div class="bus-seats">
                                                                <div class="row front">
                                                                    <div class="col-4">
                                                                        <ul class="seats">
                                                                            <li class="seat disabled">
                                                                                <label for="front-1"></label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-4">
                                                
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <ul class="seats">
                                                                            <li class="seat disabled">
                                                                                <label for="front-driver">
                                                                                    <img src="http://127.0.0.1:8000/images/steering-wheel.png"
                                                                                        class="img-fluid d-block mx-auto" width="24px">
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <ul class="seats" type="SuiteD">
                                                                                                                                        <li class="seat">
                                                                                    <input onclick="selectSeat('1D')" type="checkbox"
                                                                                        id="Suite1D" />
                                                                                    <label for="Suite1D">1D</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('2D')" type="checkbox"
                                                                                        id="Suite2D" />
                                                                                    <label for="Suite2D">2D</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('3D')" type="checkbox"
                                                                                        id="Suite3D" />
                                                                                    <label for="Suite3D">3D</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('4D')" type="checkbox"
                                                                                        id="Suite4D" />
                                                                                    <label for="Suite4D">4D</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('5D')" type="checkbox"
                                                                                        id="Suite5D" />
                                                                                    <label for="Suite5D">5D</label>
                                                                                </li>
                                                                                                    </ul>
                                                                    </div>
                                                                    <div class="col-4">
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <ul class="seats" type="SuiteB">
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('1B')" type="checkbox"
                                                                                        id="Suite1B" />
                                                                                    <label for="Suite1B">1B</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('2B')" type="checkbox"
                                                                                        id="Suite2B" />
                                                                                    <label for="Suite2B">2B</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('3B')" type="checkbox"
                                                                                        id="Suite3B" />
                                                                                    <label for="Suite3B">3B</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('4B')" type="checkbox"
                                                                                        id="Suite4B" />
                                                                                    <label for="Suite4B">4B</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('5B')" type="checkbox"
                                                                                        id="Suite5B" />
                                                                                    <label for="Suite5B">5B</label>
                                                                                </li>
                                                                                                    </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                                                        
                                                        <h4 class="text-center">Top Floor</h4>
                                                        <div class="bus">
                                                            <div class="bus-info">
                                                                <h5 class="text-center">Suite</h5>
                                                            </div>
                                                
                                                            <div class="bus-door"></div>
                                                            <div class="bus-seats">
                                                                <div class="row">
                                                                    <div class="col-4">
                                                                        <ul class="seats" type="SuiteC">
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('1C')" type="checkbox"
                                                                                        id="Suite1C" />
                                                                                    <label for="Suite1C">1C</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('2C')" type="checkbox"
                                                                                        id="Suite2C" />
                                                                                    <label for="Suite2C">2C</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('3C')" type="checkbox"
                                                                                        id="Suite3C" />
                                                                                    <label for="Suite3C">3C</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('4C')" type="checkbox"
                                                                                        id="Suite4C" />
                                                                                    <label for="Suite4C">4C</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('5C')" type="checkbox"
                                                                                        id="Suite5C" />
                                                                                    <label for="Suite5C">5C</label>
                                                                                </li>
                                                                                                    </ul>
                                                                    </div>
                                                                    <div class="col-4">
                                                
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <ul class="seats" type="SuiteA">
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('1A')" type="checkbox"
                                                                                        id="Suite1A" />
                                                                                    <label for="Suite1A">1A</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('2A')" type="checkbox"
                                                                                        id="Suite2A" />
                                                                                    <label for="Suite2A">2A</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('3A')" type="checkbox"
                                                                                        id="Suite3A" />
                                                                                    <label for="Suite3A">3A</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('4A')" type="checkbox"
                                                                                        id="Suite4A" />
                                                                                    <label for="Suite4A">4A</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('5A')" type="checkbox"
                                                                                        id="Suite5A" />
                                                                                    <label for="Suite5A">5A</label>
                                                                                </li>
                                                                                                            <li class="seat">
                                                                                    <input onclick="selectSeat('6A')" type="checkbox"
                                                                                        id="Suite6A" />
                                                                                    <label for="Suite6A">6A</label>
                                                                                </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                </div>
                                                
                                                {{-- end display seat "SUITESS" --}}
                                                {{-- start display seat "EXECUTIVE" --}}
                                                @if ($bus->armada_type === 'EXECUTIVE')
                                                    <div class="bus">
                                                        <div class="bus-info">
                                                            <h5 class="text-center">Executive 22 Seats</h5>
                                                        </div>
                                                    
                                                        <div class="bus-door"></div>
                                                    
                                                        <div class="bus-seats">
                                                            <div class="row front">
                                                                <div class="col-4">
                                                                    <ul class="seats">
                                                                        <li class="seat disabled">
                                                                            <label for="front-1"></label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-4">
                                                                    <ul class="seats">
                                                                        <li class="seat disabled">
                                                                            <label for="front-2"></label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-4">
                                                                    <ul class="seats">
                                                                        <li class="seat disabled">
                                                                            <label for="front-driver">
                                                                                <img src="http://127.0.0.1:8000/images/steering-wheel.png" class="img-fluid d-block mx-auto"
                                                                                    width="24px">
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                                    <div class="row">
                                                                <div class="col-4">
                                                                    <ul class="seats" type="ExecutiveC">
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('1C')"
                                                                                    id="Executive1C" />
                                                                                <label for="Executive1C">1C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('2C')"
                                                                                    id="Executive2C" />
                                                                                <label for="Executive2C">2C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('3C')"
                                                                                    id="Executive3C" />
                                                                                <label for="Executive3C">3C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('4C')"
                                                                                    id="Executive4C" />
                                                                                <label for="Executive4C">4C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('5C')"
                                                                                    id="Executive5C" />
                                                                                <label for="Executive5C">5C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('6C')"
                                                                                    id="Executive6C" />
                                                                                <label for="Executive6C">6C</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('7C')"
                                                                                    id="Executive7C" />
                                                                                <label for="Executive7C">7C</label>
                                                                            </li>
                                                                                        </ul>
                                                                </div>
                                                                <div class="col-4">
                                                                    <ul class="seats" type="ExecutiveB">
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('1B')"
                                                                                    id="Executive1B" />
                                                                                <label for="Executive1B">1B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('2B')"
                                                                                    id="Executive2B" />
                                                                                <label for="Executive2B">2B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('3B')"
                                                                                    id="Executive3B" />
                                                                                <label for="Executive3B">3B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('4B')"
                                                                                    id="Executive4B" />
                                                                                <label for="Executive4B">4B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('5B')"
                                                                                    id="Executive5B" />
                                                                                <label for="Executive5B">5B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('6B')"
                                                                                    id="Executive6B" />
                                                                                <label for="Executive6B">6B</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('7B')"
                                                                                    id="Executive7B" />
                                                                                <label for="Executive7B">7B</label>
                                                                            </li>
                                                                                        </ul>
                                                                </div>
                                                                <div class="col-4">
                                                                    <ul class="seats" type="ExecutiveA">
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('1A')"
                                                                                    id="Executive1A" />
                                                                                <label for="Executive1A">1A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('2A')"
                                                                                    id="Executive2A" />
                                                                                <label for="Executive2A">2A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('3A')"
                                                                                    id="Executive3A" />
                                                                                <label for="Executive3A">3A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('4A')"
                                                                                    id="Executive4A" />
                                                                                <label for="Executive4A">4A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('5A')"
                                                                                    id="Executive5A" />
                                                                                <label for="Executive5A">5A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('6A')"
                                                                                    id="Executive6A" />
                                                                                <label for="Executive6A">6A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('7A')"
                                                                                    id="Executive7A" />
                                                                                <label for="Executive7A">7A</label>
                                                                            </li>
                                                                                                <li class="seat">
                                                                                <input type="checkbox" onclick="selectSeat('8A')"
                                                                                    id="Executive8A" />
                                                                                <label for="Executive8A">8A</label>
                                                                            </li>
                                                                                        </ul>
                                                                </div>
                                                            </div>
                                                            <div class="row back">
                                                                <div class="col-4">
                                                                    <div class="bus-toilet">
                                                                        <img src="http://127.0.0.1:8000/images/toilet.png" class="img-fluid d-block mx-auto" width="32px">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="bus-exit">
                                                            <h6>EXIT</h6>
                                                        </div>
                                                    </div>
                                                @endif
                                                {{-- end display seat "EXECUTIVE" --}}
                                            </div>
                                            <div class="col-md-6">
                                                {{-- <div class="row"> --}}
                                                    {{-- <div class="col-md-6"> --}}
                                                        <h6 class="font-weight-bold text-uppercase">Detail Seat & Travel Type</h6>
                                                        <small class="font-weight-bold text-warning">Available Seats : <span id="available_seats"></span> Seat(s)</small>
                                                    {{-- </div> --}}
                                                    {{-- <div class="col-md-6">
                                                        <a href="" class="btn btn-danger" data-toggle="modal" data-target="#tambahData">
                                                            Tambah Data Penumpang
                                                        </a>
                                                    </div> 
                                                </div> --}}
                                                <hr>
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label id="customerPasengger" style="display: none">Nama Penumpang</label>
                                                        </div>
                                                        <div class="col-md-8 form-group">
                                                            <input id="customerPasengger1" style="display: none" type="text" name="name" class="form-control form-control-sm bg-transparent required" placeholder="">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label id="nikPasengger" style="display: none">NIK Penumpang</label>
                                                        </div>
                                                        <div class="col-md-8 form-group">
                                                            <input id="nikPasengger1" style="display: none" type="text" name="nik" class="form-control form-control-sm bg-transparent required" placeholder="">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Pick Point <span style="color: red">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group">
                                                            <select name="pick_point_id" id="pick_point_id" class="form-control form-control-sm bg-transparent">
                                                                @foreach ($pickpoints as $pp)
                                                                    <option value="{{ $pp->id }}">{{ $pp->pick_point_origin }} - {{ $pp->pick_point_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Arrival Point <span style="color: red">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group">
                                                            <select name="arrival_point_id" id="arrival_point_id" class="form-control form-control-sm bg-transparent">
                                                                @foreach ($pickpoints as $pp)
                                                                    <option value="{{ $pp->id }}">{{ $pp->pick_point_origin }} - {{ $pp->pick_point_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Travel Type <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group">
                                                            <input type="text" id="travel_type_name" readonly class="form-control form-control-sm bg-transparent required" name="travel_type_name" value="{{ $bus->armada_type }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Travel Price <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group ">
                                                            <input type="text" id="travel_price" readonly class="form-control form-control-sm bg-transparent required" name="travel_price" value="{{ $bus->armada_type === 'SUITESS' ? '600.000' : '375.000' }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Selected Seat <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group ">
                                                            <input type="text" id="travel_selected_seat" readonly class="form-control form-control-sm bg-transparent required" name="travel_selected_seat">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Passenger(s) <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group ">
                                                            <input type="text" id="travel_passenger" readonly class="form-control form-control-sm bg-transparent required" name="travel_passenger">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Total Price <span style="color:red;">*</span></label>
                                                        </div>
                                                        <div class="col-md-8 form-group ">
                                                            <input type="text" id="travel_total_price" class="form-control form-control-sm bg-transparent required" name="travel_total_price" readonly>
                                                            <input type="hidden" id="travel_detail_total_cost" name="travel_detail_total_cost" readonly>
                                                        </div>
                                                        <div class="col-md-12 form-group w-100 text-right ">
                                                            @if (Auth::guard('customer')->check())
                                                                <button class="btn btn-danger submit_transaction_reguler" style="display:none" type="submit">Pesan Tiket</button>
                                                            @elseif(!Auth::guard('customer')->check())
                                                                <button class="btn btn-danger submit_transaction_reguler" style="display:none" type="button" data-toggle="modal" data-target="#LoginRegisterModal">Pesan Tiket</button>
                                                            @endif
                                                            {{-- @endforeach --}}
                                                               
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                {{-- end detail bus --}}
                            </div>
                        </div>
                    </div>
                    {{-- END TRAVEL DETAIL --}}
                </form>
            </div>
        </section>
    @endforeach
    <section class="section armada-list">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="owl-carousel owl-theme" id="owl-carousel-3">

                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('register');
    {{-- @include('client.booking.ticket.modal'); --}}
@endsection
@push('page-scripts')
    <script src="{{ asset('script/admin/transaction/dashboard.js') }}"></script>
@endpush

