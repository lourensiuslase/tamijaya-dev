@foreach ($CuciArmada as $detail)
<div class="modal fade text-left" id="BagianEksterior-{{ $detail->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
    aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4 class="modal-title" id="modal-title">Form Checklist Bagian Eksterior</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <i class="bx bx-x"></i>
               </button>
           </div>
           <form action="/admin/perawatan-pemeliharaan/petugas-cuci/checklist-eksterior/{{ $detail->id }}" method="post"  enctype="multipart/form-data">
               @csrf
               <div class="modal-body">
                <table class="table table-bordered table-hover" id="table-armada">
                    <thead>
                    <tr class="text-center">
                        <th class="w-2p">No</th>
                        <th class="w-10p">Nama Bagian Eksterior</th>
                        <th class="w-5p">Checklist</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr class="text-center">
                            <td>1</td>
                            <td>Ban</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox">
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>2</td>
                            <td>Body</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox">
                            </td>
                        </tr>

                        {{-- validasi --}}
                        <tr class="text-center">
                            <td>*</td>
                            <td>Semua Eksterior Sudah Dicuci</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox" class="checklist-checkbox-eksterior">
                            </td>
                        </tr>
                    
                    </tbody>
                    </table>
               </div>
               <div class="modal-footer">
                   <button type="submit" id="checklistBagianEksterior" style="display: none" class="btn btn-success checklist-eksterior mr-1"><i
                           class="bx bx-save mt"></i> Submit
                   </button>
               </div>
           </form>
       </div>
   </div>
</div>
@endforeach
