@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Perawatan & Pemeliharaan</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-bus"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Petugas Cuci Peter
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between" style="background-color: #00b3ff">
                    <h4 class="card-title" style="color: black"><b>PEMELIHARAAN </b>| Form Notifikasi Cuci Armada</h4>
                </div>
                <div class="row">
                    {{-- <div class="col-md-7 mt-3">
                        <div class="card shadow-none bg-transparent border border-secondary mb-3">
                            <div class="card-body">
                                <div class="row mt-3">
                                    <div class="col-md-6 mb-2">
                                        <label class="form-label" for="formValidationName">Armada :</label>

                                        @foreach ($CuciArmada as $detail)
                                        <input class="form-control" type="text" value="{{$detail->armada_no_police}}" readonly/>
                                        @endforeach
                                    </div>

                                    <div class="col-md-6 mb-2">
                                        <label class="form-label" for="formValidationEmail">Tipe Armada :</label>
                                        @foreach ($CuciArmada as $detail)
                                        <input class="form-control" type="text" value="{{$detail->armada_type}}" readonly/>
                                        @endforeach
                                    </div>

                                    <div class="col-md-6 mb-2">
                                        <label class="form-label" for="formValidationName">Tipe Perjalanan :</label>
                                        @foreach ($CuciArmada as $detail)
                                        <input class="form-control" type="text" value="{{$detail->armada_category}}" readonly/>
                                        @endforeach
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label class="form-label" for="formValidationEmail">ID Perjalanan :</label>
                                        <input class="form-control" type="email" id="formValidationEmail"
                                               name="formValidationEmail" readonly/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5 mt-3">
                        <div class="card shadow-none bg-transparent border border-secondary mb-3">
                            <div class="card-body">
                                <div class="row mt-3">
                                    <div class="col-md-6 mb-5 mt-3">
                                        <label class="form-label" for="formValidationName">Jarak Tempuh Terakhir :</label>
                                        <input class="form-control" type="email" id="formValidationEmail"
                                               name="formValidationEmail"/>
                                    </div>
                                    <div class="col-md-6 mb-5 mt-3">
                                        <label class="form-label" for="formValidationEmail">Bar BBM :</label>
                                        @foreach ($CuciArmada as $data)
                                            <input class="form-control" type="text" id="formValidationEmail"
                                                name="formValidationEmail" value="{{ $data->status }}" readonly/>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <hr>
                <div class="card-content mt-5">
                    <div class="card-body card-dashboard">

                        <form action="" method="">
                            <div class="table-responsive mt-2" id="show-data-filter-accounting">
                                <table class="table table-bordered table-hover" id="table-armada">
                                    <thead>
                                    <tr class="text-center">
                                        <th class="w-2p">No</th>
                                        <th class="w-5p">Armada</th>
                                        <th class="w-10p">Tipe Armada</th>
                                        <th class="w-10p">Tipe Perjalanan</th>
                                        <th class="w-10p">Keluhan</th>
                                        <th class="w-10p">PIC Cuci</th>
                                        <th class="w-10p">Tanggal Cuci</th>
                                        <th class="w-15p">Checklist Bagian</th>
                                        <th class="w-10p">Status Cuci</th>
                                        {{-- <th class="w-5p"><input href="#" class="btn btn-primary mr-1"
                                                                onclick="selectAllItems()" type="checkbox">Action</input>
                                        </th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse($CuciArmada as $item)

                                        <tr class="text-center">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->armada_no_police }}</td>
                                            <td>{{ $item->armada_type }}</td>
                                            <td>{{ $item->armada_category }}</td>
                                            <td>{{ $item->keluhan }}</td>
                                            <td>{{ Auth::user()->name }}</td>
                                            <td>{{ $currentDate }}</td>
                                            <td>
                                                @if ($item->flag_interior === 1)
                                                    <div class="btn btn-outline-primary btn-sm " disabled>Interior</div>
                                                @else
                                                    <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#BagianInterior-{{ $item->id }}">Interior</a>
                                                @endif
                                                @if ($item->flag_eksterior === 1)
                                                    <div class="btn btn-outline-info btn-sm "  disabled>Eksterior</div>
                                                @else
                                                    <a href="#" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#BagianEksterior-{{ $item->id }}">Eksterior</a>
                                                @endif
                                            </td>

                                            @if($item->status_cuci == null)
                                                    <td>
                                                        <a href="{{ route('perawatan-pemeliharaan.petugas-cuci.setujui-cuci-check-fisik-layanan', $item->check_fisik_layanan_id) }}" class="badge bg-primary" >
                                                            <i class="bx bx-task"></i> Cuci  Sekarang
                                                        </a>
    {{--                                                    <a href="{{ route('perawatan-pemeliharaan.petugas-cuci.tolak-cuci-check-fisik-layanan', $item->id) }}" class="badge bg-danger">--}}
    {{--                                                        <i class="fa"></i> Tolak--}}
    {{--                                                    </a>--}}
                                                    </td>
                                                {{-- @else
                                                    <td>
                                                        <div class="badge bg-primary" >
                                                            <i class="bx bx-task"></i> Cuci  Sekarang
                                                        </div>
                                                    </td>
                                                @endif --}}
                                            @elseif($item->status_cuci == 1)
                                                <td>
                                                    <div class="badge bg-success">
                                                        <i class="bx bx-check-circle"></i> Di Setujui
                                                    </div>
                                                </td>
                                           {{-- @elseif($item->status == 2) --}}
{{--                                                <td>--}}
{{--                                                    <a href="{{ route('perawatan-pemeliharaan.petugas-cuci.tolak-cuci-check-fisik-layanan', $item->id) }}" class="badge bg-danger">--}}
{{--                                                        <i class="bx bx-reject"></i> Di Tolak--}}
{{--                                                    </a>--}}
{{--                                                </td> --}}
                                           @endif
                                            {{-- <td></td> --}}
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center">Tidak ada data Pengajuan Pencucian.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {{-- <div class="card-header  pb-0  d-flex justify-content-between">
                                <h4 class="card-title"></h4>
                                <button type="submit" class="btn btn-success mr-1"><i class="bx bx-save"></i> Submit
                                </button>
                            </div> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.perawatan-pemeliharaan.petugas-cuci.modal-detail')
    @include('admin.perawatan-pemeliharaan.petugas-cuci.modal-interior')
    @include('admin.perawatan-pemeliharaan.petugas-cuci.modal-eksterior')
@endsection

@push('page-scripts')
{{-- interior --}}
<script>
    $(document).ready(function() {
        $('.checklist-checkbox').change(function() {
            var totalChecked = $('.checklist-checkbox:checked').length;
            // console.log($('.checklist-checkbox:checked').length)
            if ($('.checklist-checkbox:checked').length === 1) {
                $('.checklist-interior').show();
                // console.log('true')
            } else if ($('.checklist-checkbox:checked').length === 0) {
                $('.checklist-interior').hide();
                // console.log('false')
            }
        });
    });
</script>

{{-- eksterior --}}
<script>
    $(document).ready(function() {
        $('.checklist-checkbox-eksterior').change(function() {
            var totalChecked = $('.checklist-checkbox-eksterior:checked').length;
            // console.log($('.checklist-checkbox-eksterior:checked').length)
            if ($('.checklist-checkbox-eksterior:checked').length === 1) {
                $('.checklist-eksterior').show();
                console.log('true')
            } else if ($('.checklist-checkbox-eksterior:checked').length === 0) {
                $('.checklist-eksterior').hide();
                console.log('false')
            }
        });
    });
</script>

@endpush
