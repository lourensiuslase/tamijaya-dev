@foreach ($CuciArmada as $detail)
<div class="modal fade text-left" id="BagianInterior-{{ $detail->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
    aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4 class="modal-title" id="modal-title">Form Checklist Bagian Interior</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <i class="bx bx-x"></i>
               </button>
           </div>
           <form action="/admin/perawatan-pemeliharaan/petugas-cuci/checklist-interior/{{ $detail->id }}" method="post"  enctype="multipart/form-data">
               @csrf
               <div class="modal-body">
                <table class="table table-bordered table-hover" >
                    <thead>
                    <tr class="text-center">
                        <th class="w-2p">No</th>
                        <th class="w-10p">Nama Bagian Interior</th>
                        <th class="w-5p">Checklist</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr class="text-center">
                            <td>1</td>
                            <td>Kursi</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox" >
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>2</td>
                            <td>Selimut</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox" >
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>3</td>
                            <td>Lantai</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox" >
                            </td>
                        </tr>
 
                        {{-- validasi --}}
                        <tr class="text-center">
                            <td>*</td>
                            <td>Semua Interior Sudah Dicuci</td>
                            <td>
                                <input type="checkbox" name="checklist" id="checklistCheckbox" class="checklist-checkbox">
                            </td>
                        </tr>
                       
                        </tbody>
                    </table>
               
               </div>
               <div class="modal-footer">
                   <button type="submit" id="checklistBagian" style="display: none" class="btn btn-success checklist-interior mr-1"><i
                           class="bx bx-save mt"></i> Submit
                   </button>
               </div>
           </form>
       </div>
   </div>
</div>
@endforeach



