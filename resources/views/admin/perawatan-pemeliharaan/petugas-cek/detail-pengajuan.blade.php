@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Perawatan & Pemeliharaan</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-bus"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Pengajuan Keluhan Armada
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between" style="background-color: #00b3ff">
                    <h4 class="card-title" style="color: black"><b>PEMELIHARAAN </b>| Form Pengajuan Keluhan Armada</h4>
                </div>
                

                <div class="card-content mt-5">
                    <div class="card-body card-dashboard">
                        <div class="row">
                            <div class="col-12">
                                <h4 style="black"><b>No. Police Armada - {{ $data->first()->armada_no_police }}</b></h4>
                            </div>
                        </div>

                            <div class="table-responsive mt-2" id="show-data-filter-accounting">
                                <table class="table table-bordered table-hover" id="table-armada">
                                    <thead>
                                    <tr class="text-center">
                                        <th class="w-2p">No</th>
                                        <th class="w-10p">Bagian</th>
                                        <th class="w-10p">Prosedur</th>
                                        <th class="w-10p">Keluhan</th>
                                        <th class="w-5p">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @php
                                            use App\Models\PerawatanPemeliharaan\PetugasCek\PengajuanKeluhan;
                                            use App\Models\MasterData\ProsedurBagian;
                                        @endphp
                                        @foreach ($detailPengajuanMontir as $item)
                                        @php
                                            $keluhan = PengajuanKeluhan::where('id_prosedur_bagian', $item->id && 'id_armada', $item->id_armada)->get();
                                            $kel = ProsedurBagian::where('id', $item->id)->get();
                                        @endphp
                                            <form action="{{ route('pengajuan-keluhan') }}" method="post">
                                            @csrf
                                                <input type="hidden" name="id_armada" value="{{ $id_armada }}">
                                                <input type="hidden" name="id_prosedur_bagian" value="{{ $item->id }}">
                                                <tr class="text-center">
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->prosedur_bagian_name }}</td>
                                                    <td>
                                                        @if ($keluhan->isNotEmpty())
                                                        {{-- <input type="text" value="{{ $keluhan->first()->deskripsi }}"> --}}
                                                            <div>{{ $keluhan->first()->deskripsi }}</div>
                                                        @else
                                                            <textarea class="form-control" name="deskripsi"> </textarea>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($keluhan->isNotEmpty())
                                                            <div class="btn btn-success">Terkirim</div>
                                                        @else 
                                                            <button type="submit" class="btn btn-primary submit-keluhan">Submit</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </form>
                                        @endforeach
                                    
                                    </tbody>
                                </table>


                            </div>
                            <div class="card-header  pb-0  d-flex justify-content-between">
                                <h4 class="card-title"></h4>
                                {{-- <button type="submit" class="btn btn-success mr-1"><i class="bx bx-save"></i> Submit
                                </button> --}}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


