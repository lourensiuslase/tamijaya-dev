@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Perawatan & Pemeliharaan</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-bus"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Petugas Cek armada
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between" style="background-color: #00b3ff">
                    <h4 class="card-title" style="color: black"><b>PEMELIHARAAN </b>| Form Notifikasi Cek Armada</h4>
                </div>

                <div class="card-content mt-5">
                    <div class="card-body card-dashboard">

                        <form action="" method="">
                            <div class="table-responsive mt-2" id="show-data-filter-accounting">
                                <table class="table table-bordered table-hover" id="table-armada">
                                    <thead>
                                    <tr class="text-center">
                                        <th class="w-2p">No</th>
                                        <th class="w-3p">Armada</th>
                                        <th class="w-10p">Tipe Armada</th>
                                        <th class="w-10p">Tipe Perjalanan</th>
                                        <th class="w-10p">Keluhan</th>
                                        <th class="w-10p">PIC Cek</th>
                                        <th class="w-10p">Tanggal Cek</th>
                                        <th class="w-10p">Pengajuan</th>
                                        <th class="w-10p">Status Cek</th>
                                        {{-- <th class="w-10p">Action</th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse($CekArmada as $item)

                                        <tr class="text-center">
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->armada_no_police }}</td>
                                            <td>{{ $item->armada_type }}</td>
                                            <td>{{ $item->armada_category }}</td>
                                            <td>{{ $item->keluhan }}</td>
                                            <td>{{ Auth::user()->name }}</td>
                                            <td>{{ $currentDate }}</td>

                                           
                                           <td>
                                                <a href="{{ route('detail-pengajuan-montir', $item->id_armada) }}" class="badge bg-primary mr-1">
                                                    <i class="bx bx-plus-circle"></i> Pengajuan
                                                </a>
                                           </td>
                                           @if($item->status_cek == null)
                                           <td>
                                               <a href="{{ route('perawatan-pemeliharaan.petugas-cek.setujui-cek-check-fisik-layanan', $item->check_fisik_layanan_id) }}" class="badge bg-warning ">
                                                   <i class="fa fa-check"></i> Laporkan ke SPV
                                               </a>
                                           </td>
                                       @elseif ($item->status_cek == 2)
                                           <td>
                                               <div class="badge bg-warning">
                                                   <i class="bx bx-check-circle"></i> Dilaporkan ke SPV
                                               </div>
                                           </td>
                                       @elseif($item->status_cek == 3)
                                           <td>
                                               <div class="badge bg-success">
                                                   <i class="bx bx-check-circle"></i> Approved
                                               </div>
                                           </td>
                                       @else
                                           <td>
                                               <div class="badge bg-success">
                                                   <i class="bx bx-check-circle"></i> Approved
                                               </div>
                                           </td>
                                      @endif

                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="9" class="text-center">Tidak ada data Pengecekan Armada.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>


                            </div>
                            <div class="card-header  pb-0  d-flex justify-content-between">
                                <h4 class="card-title"></h4>
                                {{-- <button type="submit" class="btn btn-success mr-1"><i class="bx bx-save"></i> Submit
                                </button> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.perawatan-pemeliharaan.petugas-cek.modal-detail')
@endsection

