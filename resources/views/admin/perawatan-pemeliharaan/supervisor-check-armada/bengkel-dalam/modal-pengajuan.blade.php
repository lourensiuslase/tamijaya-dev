<div class="modal fade text-left" id="modalPengajuan" tabindex="-1" role="dialog" aria-labelledby="modal-title"
    aria-hidden="true">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h4 class="modal-title" id="modal-title">Form Pengajuan Sparepart</h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <i class="bx bx-x"></i>
               </button>
           </div>
           <form action="{{ route('pengajuan-sparepart') }}" method="post"  enctype="multipart/form-data">
               @csrf
               <input type="hidden" name="pic_pemohon" value="{{ Auth::user()->name }}">
               <div class="modal-body">
                <label>No Police Armada : </label>
                <div class="form-group">
                    <select id="id_armada" name="id_armada" class="form-control" >
                        @foreach ($dataArmada as $data)
                            <option value="{{ $data->id }}">{{ $data->armada_no_police }}</option>
                        @endforeach
                    </select>
                </div>
                <label>Nama Item : </label>
                <div class="form-group">
                    <select id="id_komponen" name="id_komponen" class="form-control" >
                        @foreach ($dataKomponen as $data)
                            <option value="{{ $data->id }}">{{ $data->nama_komponen }}</option>
                        @endforeach
                    </select>
                </div>
                <label>Jumlah : </label>
                <div class="form-group">
                    <input class="form-control" type="number" name="jml_permintaan" value="1">
                </div>
                <label>Deskripsi : <span style="color: red; font-size:8px">opsional</span></label>
                <div class="form-group">
                    <textarea class="form-control" name="deskripsi"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit"  class="btn btn-success mr-1"><i
                        class="bx bx-save mt"></i> Ajukan
                </button>
            </div>
           </form>
       </div>
   </div>
</div>
