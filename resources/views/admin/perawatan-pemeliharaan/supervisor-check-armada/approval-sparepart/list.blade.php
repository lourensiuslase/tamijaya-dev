@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Perawatan & Pemeliharaan</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-bus"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Supervisor Check Armada
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="toolbar row">
                        <div class="col-md-12">
                            <h4 class="card-title">Approval Sparepart</h4>
                        </div>
                    </div>
                </div>
                <div class="card-content pt-1">
{{--                    <div class="card-body card-dashboard">--}}
{{--                        <form action="">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-2">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="">Armada</label>--}}
{{--                                        <input type="text" class="form-control bg-transparent" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-2">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="">Tipe Armada</label>--}}
{{--                                        <input type="text" class="form-control bg-transparent" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-2">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="">Jarak Tempuh Terakhir</label>--}}
{{--                                        <input type="text" class="form-control bg-transparent" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-2">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="">Bar BBM</label>--}}
{{--                                        <input type="text" class="form-control bg-transparent" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
                    <div class="card-body card-dashboard">
                        <a href="#" class="btn btn-primary">Approval Keluhan</a>
                        <a href="#" class="btn btn-outline-primary">Penentuan Bengkel</a>
                    </div>

                    <div class="card-body">
                        <table class="table table-bordered table-hover" id="table-armada">
                            <thead>
                            <tr class="text-center">
                                <th class="w-2p">No</th>
                                <th class="w-3p">Armada</th>
                                <th class="w-10p">Tipe Armada</th>
                                <th class="w-10p">Tipe Perjalanan</th>
                                <th class="w-10p">Keluhan</th>
                                <th class="w-10p">PIC Cek</th>
                                <th class="w-10p">Tanggal Cek</th>
                                <th class="w-10p">Status Cek</th>
                                <th class="w-10p">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="float-right">
                            <button type="submit" class="btn btn-success mr-1"><i class="bx bx-save"></i> Submit
                            </button>
                        </div>
                    </div>
                </div>
                <p></p>
            </div>
        </div>
    </div>

@endsection
