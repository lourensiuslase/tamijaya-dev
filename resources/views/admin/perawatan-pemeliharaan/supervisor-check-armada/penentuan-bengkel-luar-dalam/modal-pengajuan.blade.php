@foreach ($SpvCheck as $item)
    <div class="modal fade text-left" id="pengajuanBengkelLuar-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
        aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Form Pengajuan Bengkel Luar</h4>
                <span style="font-weight: bold; color:red">{{ $item->armada_no_police }}</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <form action="{{ route('pengajuan-bengkel-luar') }}" method="post"  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_armada" value="{{ $item->id }}">
                <div class="modal-body">
                    <label>No Police Armada : </label>
                    <div class="form-group">
                        <input class="form-control" type="text" value="{{ $item->armada_no_police }}" disabled>
                    </div>
                    <label>Nama Bengkel Luar : </label>
                    <div class="form-group">
                        <input class="form-control" type="text" name="nama">
                    </div>
                    <label>Estimasi Harga : </label>
                    <div class="form-group">
                        <input class="form-control" type="number" name="estimasi_harga">
                    </div>
                    <label>Estimasi Selesai : </label>
                    <div class="form-group">
                        <input class="form-control" type="date" name="estimasi_selesai">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"  class="btn btn-success mr-1"><i
                            class="bx bx-save mt"></i> Ajukan
                    </button>
                </div>
            </form>
        </div>
    </div>
    </div>
@endforeach
