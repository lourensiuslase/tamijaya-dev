@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Perawatan & Pemeliharaan</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-bus"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Penentuan Perawatan Bengkel Dalam / Bengkel Luar
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between" style="background-color: #00b3ff">
                    <h4 class="card-title" style="color: black"><b>PEMELIHARAAN </b>| Form Penentuan Perawatan Bengkel Dalam / Bengkel Luar</h4>
                </div>
                <div class="card-content mt-2">
                    <div class="card-body card-dashboard">
                        <form action="" id="form-search-transaction">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href=" " class="btn   mt-1" STYLE="background-color: pink">SPAREPART ARMADA</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href=" " class="btn btn-info mt-1">LOGISTIK PERJALANAN</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive mt-2 mb-3" id="show-data-filter-accounting">
                            <table class="table table-bordered table-hover" id="table-armada">
                                <thead class="text-center">
                                <tr >
                                    <th  class="w-2p" rowspan="2">No</th>
                                    <th  class="w-10p" rowspan="2">Armada</th>
                                    <th  class="w-10p" rowspan="2">Bagian</th>
                                    <th  class="w-10p" rowspan="2">Approval Montir</th>
                                    <th  class="w-10p" rowspan="2">Approval SPV</th>
                                    <th colspan="2"  class="w-5p">Perbaikan Bengkel</th>
                                    {{-- <th  class="w-3p" rowspan="2">Nama Bengkel Luar</th> --}}
                                    <th class="w-5p" colspan="2">Status</th>
                                    <th class="w-5p" rowspan="2">Final</th>
                                </tr>
                                <tr>
                                    <td>Dalam</td>
                                    <td>Luar</td>
                                    <td>Cuci</td>
                                    <td>Cek</td>
                                </tr>
                                </thead>
                                <tbody>
<<<<<<< HEAD
                                <tr>
                                    <td colspan="11" style="background-color: #f8d5b0">PENENTUAN BENGKEL LUAR ATAU DALAM</td>
                                </tr>

                                @forelse ($SpvCheck as $item)
                                    <tr class="text-center">
                                        <td >{{ $loop->iteration }}</td>
                                        <td>{{ $item->armada_no_police }}</td>
                                        <td ><a class="badge bg-primary" href="{{ route('detail-pengajuan-montir-supervisor', $item->id_armada) }}">Detail</a></td>
                                        {{-- approval montir --}}
                                        <td >
                                            @if ($item->status_cek === 3 )
                                                <div class="badge bg-success">Approved</div>
                                            @elseif ($item->status_cek === 2)
                                                <a href="{{ route('approval-montir', $item->check_fisik_layanan_id) }}" class="badge bg-warning">Approve</a>
                                            @else 
                                                <div class="badge bg-success">Approved</div>
                                            @endif
                                        </td>
                                        <td >
                                            {{-- approval SPV --}}
                                            @if ($item->status_cek === 1 )
                                                <div class="badge bg-success">Approved</div>
                                            @else
                                                <a href="{{ route('approval-svp', $item->check_fisik_layanan_id) }}" class="badge bg-warning">Approve</a>
                                            @endif
                                        </td>
                                        <td >
                                            {{-- @dd($item->id) --}}
                                            @if ($item->status_bengkel === 2)
                                                <div class="badge bg-warning">Approve</div>
                                            @elseif ($item->status_bengkel === 1)
                                                <div class="badge bg-success">Approved</div>
                                            @else
                                                <form action="{{ route('tambah-bengkel-dalam') }}" method="post">
                                                @csrf 
                                                    <input type="hidden" name="cek_armada" value="{{ $item->id }}">
                                                    <button type="submit" class="badge bg-primary" style="border: 0">Submit</button>
                                                </form>
                                            @endif
                                        </td>
                                        {{-- ajukan bengkel luar --}}
                                        <td >
                                            @if (!$item->flag_bengkel_luar)
                                                <a href="#" class="badge bg-primary" data-toggle="modal" data-target="#pengajuanBengkelLuar-{{ $item->id }}">Ajukan</a>
                                            @elseif ($item->flag_bengkel_luar === 2)
                                                <div class="badge bg-success">Approved</div>
                                            @elseif ($item->flag_bengkel_luar === 1)
                                                <div class="badge bg-warning">Approve</div>
                                            @endif
                                        </td>
                                        {{-- <td ></td> --}}
                                        <td>
                                            @if ($item->status_cuci === 1)
                                                <div class="badge bg-success">Sudah Dicuci</div>
                                            @else
                                                <div class="badge bg-warning">Belum Dicuci</div>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->status_cek === 1)
                                                <div class="badge bg-success">Sudah Dicek</div>
                                            @else
                                                <div class="badge bg-warning">Belum Dicek</div>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->status_cek === 1 && $item->status_cuci === 1)
                                                <div class="badge bg-success">Siap Jalan</div>
                                            @else
                                                <div class="badge bg-warning">Belum Siap</div>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <td>Tidak Pengajuan Keluhan dari Cek Rutin</td>
                                @endforelse
=======
{{--                                @forelse ($SpvCheck as $item)--}}
{{--                                    <tr class="text-center">--}}
{{--                                        <td >{{ $loop->iteration }}</td>--}}
{{--                                        <td>{{ $item->armada_no_police }}</td>--}}
{{--                                        <td ><a class="badge bg-primary" href="{{ route('detail-pengajuan-montir-supervisor', $item->id_armada) }}">Detail</a></td>--}}
{{--                                        --}}{{-- approval montir --}}
{{--                                        <td >--}}
{{--                                            @if ($item->status_cek === 3 )--}}
{{--                                                <div class="badge bg-success">Approved</div>--}}
{{--                                            @elseif ($item->status_cek === 2)--}}
{{--                                                <a href="{{ route('approval-montir', $item->check_fisik_layanan_id) }}" class="badge bg-warning">Approve</a>--}}
{{--                                            @else --}}
{{--                                                <div class="badge bg-success">Approved</div>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td >--}}
{{--                                            --}}{{-- approval SPV --}}
{{--                                            @if ($item->status_cek === 1 )--}}
{{--                                                <div class="badge bg-success">Approved</div>--}}
{{--                                            @else--}}
{{--                                                <a href="{{ route('approval-svp', $item->check_fisik_layanan_id) }}" class="badge bg-warning">Approve</a>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td >--}}
{{--                                            --}}{{-- @dd($item->id) --}}
{{--                                            @if ($item->status_bengkel === 2)--}}
{{--                                                <div class="badge bg-warning">Approve</div>--}}
{{--                                            @elseif ($item->status_bengkel === 1)--}}
{{--                                                <div class="badge bg-success">Approved</div>--}}
{{--                                            @else--}}
{{--                                                <form action="{{ route('tambah-bengkel-dalam') }}" method="post">--}}
{{--                                                @csrf --}}
{{--                                                    <input type="hidden" name="cek_armada" value="{{ $item->id }}">--}}
{{--                                                    <button type="submit" class="badge bg-primary" style="border: 0">Submit</button>--}}
{{--                                                </form>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td ><a class="badge bg-warning" href="">Submit</a></td>--}}
{{--                                        <td ></td>--}}
{{--                                        <td>--}}
{{--                                            @if ($item->status_cuci === 1)--}}
{{--                                                <div class="badge bg-success">Sudah Dicuci</div>--}}
{{--                                            @else--}}
{{--                                                <div class="badge bg-warning">Belum Dicuci</div>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if ($item->status_cek === 1)--}}
{{--                                                <div class="badge bg-success">Sudah Dicek</div>--}}
{{--                                            @else--}}
{{--                                                <div class="badge bg-warning">Belum Dicek</div>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @if ($item->status_cek === 1 && $item->status_cuci === 1)--}}
{{--                                                <div class="badge bg-success">Siap Jalan</div>--}}
{{--                                            @else--}}
{{--                                                <div class="badge bg-warning">Belum Siap</div>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
{{--                                @empty--}}
{{--                                    <td>Tidak Pengajuan Keluhan dari Cek Rutin</td>--}}
{{--                                @endforelse--}}
>>>>>>> 4788314ec9447449139ce18cd0e3877ad9a240f4

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('admin.perawatan-pemeliharaan.supervisor-check-armada.penentuan-bengkel-luar-dalam.modal-pengajuan')
@endsection
