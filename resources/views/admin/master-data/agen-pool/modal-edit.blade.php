@foreach($items as $item)
    <div class="modal fade text-left" id="EditAgenpool-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-title">Form Edit Agen Pool</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <form action="{{ route('agen-pool.update', $item->id) }}" method="post"  enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="modal-body">
                        {{-- <label>Gambar : </label>
                        <div class="form-group">
                            <input class="form-control" type="file" name="image" accept="image/*" id="imgInp">
                            <img id="blah" class="w-50">
                        </div> --}}
                        <label>Nama Agen : </label>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="SILAHKAN ISI DATA NAMA AGEN" name="name" value="{{ $item->name ?? old('name') }}" required>
                        </div>
                        <label>Lokasi : </label>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="SILAHKAN ISI DATA LOKASI" name="lokasi" value="{{ $item->lokasi ?? old('lokasi') }}" required>
                        </div>
                        <label>No. Handphone : </label>
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="SILAHKAN ISI DATA NO.HANDPHONE" name="number_phone" value="+{{ $item->number_phone ?? old('number_phone') }}" required>
                        </div>
                        <label>Area : </label>
                        <div class="form-group">
                            <select class="form-control" name="area" id="area">
                                @if ($item->area != NULL)
                                    <option value="{{ $item->area }}" selected>AREA {{ $item->area }}</option>
                                    <option value="YOGYAKARTA">Area Yogyakarta</option>
                                    <option value="KLATEN">Area Klaten</option>
                                    <option value="SOLO">Area Solo</option>
                                    <option value="BALI">Area  Bali</option>
                                @else
                                    <option value="NULL" selected>SILAHKAN PILIH AREA AGEN</option>
                                    <option value="YOGYAKARTA">Area Yogyakarta</option>
                                    <option value="KLATEN">Area Klaten</option>
                                    <option value="SOLO">Area Solo</option>
                                    <option value="BALI">Area  Bali</option>
                                @endif
                            </select>
                        </div>
                        <label>Link Maps : </label>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="SILAHKAN ISI DATA LINK MAPS" name="link_maps" value="{{ $item->link_maps ?? old('link_maps') }}" required>
                        </div>
                        <label>Deskripsi : </label>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="SILAHKAN ISI DATA DESKRIPSI" name="deskripsi" cols="30" rows="10">{{ $item->deskripsi ?? '' }}</textarea>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="submit"  class="btn btn-success mr-1"><i
                                class="bx bx-save mt"></i> Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endforeach
