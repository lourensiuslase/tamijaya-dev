@extends('admin.layouts.app')
@section('content-header')
<div class="content-header-left col-12 mb-2 mt-1">
    <div class="row breadcrumbs-top">
        <div class="col-12">
            <h5 class="content-header-title float-left pr-1 mb-0">Agen Pool</h5>
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb p-0 mb-0">
                    <li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>
                    </li>
                    <li class="breadcrumb-item active">Agen Pool
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header" style="background-color: #00b3ff">
                <div class="toolbar row ">
                    <div class="col-md-12 d-flex">
                        <h4 class="card-title">List Data Master Agen Pool</h4>
                        <div class="col ml-auto">
                            <div class="dropdown float-right">
                                <a href="" class="btn btn-primary mr-1" data-toggle="modal"
                                       data-target="#TambahAgenpool"><i class="bx bx-plus-circle"></i>Tambah Data</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content mt-2">
                <div class="card-body card-dashboard">
                    <div class="table">
                            <div class="text-center">
                                <table class="table table-bordered table-hover table-responsive-lg" id="table-agenpool">
                                    <thead>
                                        <tr>
                                            <th class="w-5p">No</th>
                                            {{-- <th width="20%">Gambar</th> --}}
                                            <th>Nama Agen</th>
                                            <th>Lokasi</th>
                                            <th>No. Handphone</th>
                                            <th>Area</th>
                                            <th>Link Maps</th>
                                            <th>Deskripsi</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @forelse($items as $item)
                                        <tr id="row-agenpool-">
                                            <td>{{ $loop->iteration }}</td>
                                            {{-- <td>
                                                <img class="w-50" src="{{ asset("storage/" . $item->image) }}" alt="">
                                            </td> --}}
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->lokasi }}</td>
                                            <td>+{{ $item->number_phone }}</td>
                                            <td>{{ $item->area }}</td>
                                            <td>{{ $item->link_maps }}</td>
                                            <td>{{ $item->deskripsi }}</td>
                                            <td class="text-center">
                                                <div class="d-flex">
                                                    <div class="badge-circle badge-circle-sm badge-circle-warning mr-1 pointer"
                                                        data-toggle="modal"
                                                        data-target="#EditAgenpool-{{ $item->id }}">
                                                        <i class="bx bx-edit font-size-base"></i>
                                                    </div>
                                                    <div class="badge-circle badge-circle-sm badge-circle-danger pointer"
                                                        onclick="manageData('delete', {{ $item->id }})"
                                                        >
                                                        <i class="bx bx-trash font-size-base"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                      @empty
                                        <tr>
                                            <td colspan="7" class="text-center">
                                                Tidak Terdapat Data Agen Pool
                                            </td>
                                        </tr>
                                      @endforelse
                                    </tbody>
                                    </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.master-data.agen-pool.modal-tambah')
@include('admin.master-data.agen-pool.modal-edit')
@endsection

@push('page-scripts')
<script src="{{ asset('script/admin/master-data/index.js') }}"></script>

{{-- preview image --}}
<script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
            if (file) {
                blah.src = URL.createObjectURL(file)
            }
    }
</script>

<script>
    $(document).ready(function () {
        $("#table-bagian").DataTable();
    });


    @if(session('pesan-berhasil'))
    Swal.fire({
        icon: 'success',
        title: 'Berhasil',
        text: '{{ session("pesan-berhasil") }}'
    });
    @elseif(session('pesan-gagal'))
    Swal.fire({
        icon: 'error',
        title: 'Gagal',
        text: '{{ session("pesan-gagal") }}'
    });
    @endif

@endpush
