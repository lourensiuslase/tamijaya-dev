<div class="modal fade text-left" id="TambahBahanbakar" tabindex="-1" role="dialog" aria-labelledby="modal-title"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Form Tambah Bahan Bakar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <form action="{{ route('bahanbakar.store') }}" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label>Status : </label>
                    <div class="form-group">
                        <select id="status" name="status" class="form-control" >
                            <option value="">Silahkan Pilih Kondisi Bahan Bakar</option>
                            <option value="PENUH">Penuh</option>
                            <option value="SETENGAH">Setengah</option>
                            <option value="HABIS">Habis</option>
                        </select>
                    </div>
                    <label>Deskripsi : </label>
                    <div class="form-group">
                        <select id="deskripsi" name="deskripsi" class="form-control" >
                            <option value="">Silahkan Pilih Presentase Bahan Bakar</option>
                            <option value="Indikator Lebih Dari 75%">Indikator Lebih Dari 75%</option>
                            <option value="Indikator Diantara 50% - 75%">Indikator Diantara 50% - 75%</option>
                            <option value="Indikator Kurang Dari 50%">Indikator Kurang Dari 50%</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"  class="btn btn-success mr-1"><i
                            class="bx bx-save mt"></i> Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
