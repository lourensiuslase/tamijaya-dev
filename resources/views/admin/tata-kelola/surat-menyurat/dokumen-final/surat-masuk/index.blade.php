@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Surat Menyurat</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href=" "><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Tata Kelola
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="toolbar row ">
                        <div class="col-md-12 d-flex">
                            <h6 class="card-title">Dokumen Final</h6>
                        </div>
                    </div>
                </div>
                <div class="card-content pt-1">
                    <div class="card-body">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-justified" id="tabDriverConductor" role="tablist">
                                <li class="nav-item current">
                                    <a class="nav-link active" id="logbook-keluar-tab-justified"
                                       data-toggle="tab" href="#" role="tab"
                                       aria-controls="logbook-keluar-data"
                                       aria-selected="true">
                                        Logbook
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="sparepart-keluar-tab-justified"
                                       data-toggle="tab" href="#" role="tab"
                                       aria-controls="sparepart-keluar-data"
                                       aria-selected="true">
                                        Sparepart
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="logistik-keluar-tab-justified"
                                       data-toggle="tab" href="#" role="tab"
                                       aria-controls="logistik-keluar-data"
                                       aria-selected="true">
                                        Logistik
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="get">
                            <div class="row ">
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <label for="tanggal_input">Tanggal Input</label>
                                        <input type="date" id="tanggal_input" name="tanggal_input"
                                               value="{{ $params['tanggal_input'] ?? '' }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <label for="tanggal_surat">Tanggal Surat</label>
                                        <input type="date" id="tanggal_surat" name="tanggal_surat"
                                               value="{{ $params['tanggal_surat'] ?? '' }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <label for="penerima_surat">Penerima Surat</label>
                                        <select class="form-control" name="penerima_surat">
                                            <option disabled selected>Pilih Penerima Surat</option>
                                            @foreach($dokumen as $jbt)
                                                @php
                                                    $selected = ($params['penerima_surat'] == $jbt->penerima_surat) ? "selected" : "";
                                                @endphp
                                                <option
                                                    value="{{ $jbt->penerima_surat }}" {{ $selected }}>{{ $jbt->penerima_surat }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <label for="" style="color: white">Filter</label><br>
                                        <button class="btn btn-primary"><i class="bx bx-filter"></i> Filter</button>
                                        <a href="{{ route('data-kelola.surat-menyurat.list-dokumen-final') }}"
                                           class="btn btn-warning"><i class="bx bx-reset"></i> Reset</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <input type="hidden" value=" ">
                            @php
                                use Carbon\Carbon;
                                 Carbon::setLocale('id');
                            @endphp
                            <table class="table table-bordered table-hover" id="table-surat-masuk">
                                <thead>
                                <tr class="text-uppercase text-center">
                                    <th class="w-3p">No</th>
                                    <th class="w-10p">Tanggal Input</th>
                                    <th class="w-10p">No Registrasi Surat</th>
                                    <th class="w-10p">Nomor Surat</th>
                                    <th class="w-10p">Tanggal Surat</th>
                                    <th class="w-10p">Pengirim 👉 Penerima</th>
                                    <th class="w-10p">Keterangan</th>
                                    <th class="w-3p">Aksi</th>
                                </tr>
                                </thead>
                                <tbody id="show-data-employee">
                                @forelse($dokumen as $item)
                                    <tr class="text-center">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ Carbon::parse($item->tanggal_input)->translatedFormat('l, d F Y') }}</td>

                                        <td>{{ $item->no_registrasi_surat }}</td>
                                        <td>{{ $item->no_surat }}</td>

                                        <td>{{ Carbon::parse($item->tanggal_surat)->translatedFormat('l, d F Y') }}</td>
                                        <td>({{ $item->pengirim_surat }}) 👉 ({{ $item->penerima_surat }})</td>
                                        <td>{{ $item->keterangan_surat }}</td>
                                        <td class="text-center">
                                            <div class="d-flex">
                                                <div
                                                    class="badge-circle badge-circle-sm badge-circle-primary mr-1 pointer"
                                                    data-toggle="modal"
                                                    data-target="#DetailSurat-{{ $item->id }}">
                                                    <i class="bx bx-info-circle font-size-base"></i>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="col-md-2">
                                                        @if($item)
                                                            <form
                                                                action="{{ route('data-kelola.surat-menyurat.archieve-data') }}"
                                                                class="d-inline delete-form" method="post">
                                                                @csrf
                                                                <input type="hidden" name="id_qs[]"
                                                                       value="{{ $item->id }}">
                                                                <button type="submit"
                                                                        class="badge-circle badge-circle-sm badge-circle-danger mr-1 pointer"
                                                                        id="btn-submit-pekerjaan-sm"
                                                                        onclick="event.preventDefault(); showConfirmationModal(this);">
                                                                    <i class="bx bx-trash"></i>
                                                                </button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="8" class="text-center">Data Dokumen Final</td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('page-scripts')
@endpush




