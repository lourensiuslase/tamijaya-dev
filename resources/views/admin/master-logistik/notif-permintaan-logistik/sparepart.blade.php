<div class="table-responsive">
    <table class="table datatables table-bordered table-hover"
           id="table-notif-sparepart-logistik">
        <thead>
        <tr>
            <th rowspan="2" class="w-3p">No</th>
            <th rowspan="2" class="w-10p">Pic Pemohon</th>
            <th rowspan="2" class="w-10p">Armada</th>
            <th rowspan="2" class="w-10p">Nama Item</th>
            <th rowspan="2" class="w-5p">Sub Bagian</th>
            <th rowspan="2" class="w-5p">Bagian</th>
            <th rowspan="2" class="w-5p">Stock Gudang</th>
            <th rowspan="2" class="w-5p">Jumlah Permintaan</th>
            {{-- <th colspan="2" class="w-10p text-center">Status Ketersediaan</th> --}}
            {{-- <th rowspan="2" class="w-5p">Ajukan Pembeli</th> --}}
            <th colspan="2" class="w-10p text-center">Status</th>
        </tr>
        <tr>
            <th>Tolak</th>
            <th>Terima</th>
        </tr>
        </thead>
        <tbody>

            @forelse ($dataPengajuan as $item)
                <tr class="text-center" style="color: #880000; font-style: italic;">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->pic_pemohon }}</td>
                    <td>{{ $item->armada_no_police }}</td>
                    <td>{{ $item->nama_komponen }}</td>
                    <td>{{ $item->nama_sub_bagian }}</td>
                    <td>{{ $item->nama_bagian }}</td>
                    <td>{{ $item->stok }}</td>
                    <td>{{ $item->jml_permintaan }}</td>
                    {{-- <td>{{ $item->stok >= 1 ? 'Tersedia' : '' }}</td>
                    <td>{{ $item->stok = 0 ?? 'Tidak Tersedia' }}</td> --}}
                    {{-- <td><a href="" class="badge bg-primary">Ajukan</a></td> --}}
                    <td>
                        @if ($item->status_pengajuan_sparepart === 2)
                            <div class="badge bg-danger">Ditolak</div>
                        @elseif ($item->status_pengajuan_sparepart === 1)
                            <a href="{{ route('tolak-sparpart', $item->id) }}" class="badge bg-danger">Tolak</a>
                        @else
                            <a href="{{ route('tolak-sparpart', $item->id) }}" class="badge bg-danger">Tolak</a>
                        @endif
                    </td>
                    <td> 
                        @if ($item->status_pengajuan_sparepart === NULL)
                            <a href="{{ route('setujui-sparpart', $item->id) }}" class="badge bg-warning">Terima</a>
                        @elseif ($item->status_pengajuan_sparepart === 2)
                            <a href="{{ route('setujui-sparpart', $item->id) }}" class="badge bg-warning">Terima</a>
                        @else
                            <div class="badge bg-success">Diterima</div>
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="12">Tidak Ada Pengajuan Sparepart</td>
                </tr>
            @endforelse

        </tbody>
    </table>
</div>
{{-- <div class="float-right">
    <button class="btn btn-success" onclick="pembelianSparepart()"><i class='bx bxs-message-rounded-dots'></i> Pembelian Sparepart</button>
    <button class="btn btn-primary" onclick="notifKePerawatan()"><i class='bx bxs-message' ></i> Notif Ke Bag Perawatan</button>
</div> --}}
<br>
