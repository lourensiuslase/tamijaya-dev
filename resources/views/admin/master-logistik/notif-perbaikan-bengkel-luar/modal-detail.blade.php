@php
    use App\Models\PerawatanPemeliharaan\PetugasCek\PengajuanKeluhan;
@endphp
@foreach ($pengajuanBengkelLuar as $item)
    <div class="modal fade text-left" id="DetailKerusakan-{{ $item->id_armada }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
        aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">List Detail Kerusakan - {{ $item->armada_no_police }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover">
                    <thead class="text-center">
                    <tr >
                        <th  class="w-2p">No</th>
                        <th  class="w-10p">Bagian Cek</th>
                        <th  class="w-10p">Prosedur Bagian</th>
                        <th  class="w-10p">Deskripsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                        @php
                        $data = PengajuanKeluhan::select('*')
                                ->join('prosedur_bagian', 'prosedur_bagian.id', '=', 'pengajuan_keluhan.id_prosedur_bagian')
                                ->join('armadas', 'armadas.id', '=', 'pengajuan_keluhan.id_prosedur_bagian')
                                ->leftjoin('bagian_cek', 'prosedur_bagian.id_bagian_cek', '=', 'bagian_cek.id')
                                ->where('id_prosedur_bagian', $item->id_prosedur_bagian )
                                ->where( 'id_armada', $item->id_armada )
                                ->get();
                        @endphp
                    @forelse ($data as $item)
                    {{-- @php
                        $data = PengajuanKeluhan::select('*')->join('prosedur_bagian', 'prosedur_bagian.id', '=', 'pengajuan_keluhan.id_prosedur_bagian')->where('id_prosedur_bagian', $item->id_prosedur_bagian && 'id_armada', $item->id_armada)->get();
                    @endphp
                        <tr class="text-center">
                            <td >{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $data->first()->prosedur_bagian_name }}</td>
                            <td>{{ $data->deskripsi }}</td>
                        </tr> --}}
                        
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $data->first()->prosedur_bagian_name ? $data->first()->prosedur_bagian_name : 'Data Not Found' }}</td>
                            <td>{{ $data->first()->deskripsi ? $data->first()->deskripsi : 'Data Not Found' }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="10">Tidak Pengajuan Keluhan</td>
                        </tr>
                    @endforelse
    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
@endforeach