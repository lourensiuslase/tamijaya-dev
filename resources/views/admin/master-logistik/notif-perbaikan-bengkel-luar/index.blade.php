@extends('admin.layouts.app')
@section('content-header')
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Logistik</h5>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb p-0 mb-0">
                        <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active">Perbaikan Bengkel Luar
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="toolbar row ">
                        <div class="col-md-12 d-flex">
                            <h4 class="card-title">Perbaikan Bengkel Luar</h4>
                        </div>
                    </div>
                </div>
                <div class="card-content pt-1">
                    <div class="card-body card-dashboard">
                        <div class="table-responsive">
                            .<table class="table datatables table-bordered table-hover">
                                <thead>
                                <tr class="text-center">
                                    <th class="w-3p">No</th>
                                    <th class="w-10p">Armada</th>
                                    <th class="w-10p">Keluhan</th>
                                    <th class="w-15p">Nama Bengkel Luar</th>
                                    <th class="w-5p">Tanggal Masuk</th>
                                    <th class="w-5p">Estimasi Harga (Rp)</th>
                                    <th class="w-5p">Estimasi Waktu Selesai</th>
                                    <th class="w-5p">Status</th>
                                    <th class="w-5p">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($pengajuanBengkelLuar as $item)
                                    <tr class="text-center">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->armada_no_police }}</td>
                                        <td><a href="#" class="badge bg-primary" data-toggle="modal" data-target="#DetailKerusakan-{{ $item->id_armada }}">Detail Kerusakan</a></td>
                                        <td>{{ $item->name_bengkel_luar }}</td>
                                        <td>{{ $item->tgl_ajukan }}</td>
                                        <td>{{ number_format($item->estimasi_harga) }}</td>
                                        <td>{{ $item->estimasi_selesai }}</td>
                                        <td>
                                            @if ($item->flag_bengkel_luar === 1)
                                                <div class="badge bg-warning">Pengajuan</div>
                                            @elseif ($item->flag_bengkel_luar === 2)
                                                <div class="badge bg-success">Diterima</div>
                                            @elseif ($item->flag_bengkel_luar === 3)
                                                <div class="badge bg-danger">Ditolak</div>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="" class="badge bg-info" data-toggle="modal" data-target="#AjukanBL-{{ $item->id_armada }}">Ajukan</a>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>Tidak Ada Pengajuan Bengkel Luar</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.master-logistik.notif-perbaikan-bengkel-luar.modal-ajukan')
    @include('admin.master-logistik.notif-perbaikan-bengkel-luar.modal-detail')
@endsection

@push('page-scripts')
@endpush
