@foreach ($pengajuanBengkelLuar as $item)
    <div class="modal fade text-left" id="AjukanBL-{{ $item->id_armada }}" tabindex="-1" role="dialog" aria-labelledby="modal-title"
        aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Form Pengajuan Bengkel Luar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <form action="" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label>No Police Armada : </label>
                    <div class="form-group">
                        <input class="form-control" type="text" value="{{ $item->armada_no_police }}" disabled>
                    </div>
                    <label>Estimasi Harga : </label>
                    <div class="form-group">
                        <input class="form-control" type="number" value="{{ $item->estimasi_harga }}" disabled>
                    </div>
                    <label>Estimasi Selesai : </label>
                    <div class="form-group">
                        <input class="form-control" type="text" value="{{ $item->estimasi_selesai }}" disabled>
                    </div>
                    <label>Upload PDF : </label>
                    <div class="form-group">
                        <input class="form-control" type="file" name="upload_pdf" id="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"  class="btn btn-success mr-1"><i
                            class="bx bx-save mt"></i> Ajukan
                    </button>
                </div>
            </form>
        </div>
    </div>
    </div>
@endforeach