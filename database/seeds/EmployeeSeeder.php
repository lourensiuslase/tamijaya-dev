<?php

use Illuminate\Database\Seeder;
use App\Models\HumanResource\Employee;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
        Employee::create([
            'employee_id' => '2023-EMP-01',
            'id_fingerprint' => '002',
            'employee_name' => 'Adita Rian P
',
            'id_department' => 1,
            'position_id' => 1,
            'employee_status' => 'Tetap',
            'awal_kontrak' => '2018-03-24',
            'selesai_kontrak' => '2023-03-2',
            'jenis_kelamin' => 'L',
            'tanggal_lahir' => '1999-03-02',
            'status_perkawinan' => 'Belum Kawin',
            'alamat' => 'Yogyakarta',
            'alamat_domisili' => 'Yogyakarta',
            'nik' => '352562654758568568',
            'npwp' => '54654654654654',
            'bpjs_kesehatan' => '65654645654636',
            'bpjs_ketenagakerjaan' => '5464565',
            'telepon' => '08546346346',
            'email' => 'mamiek@gmail.com',
            'rekening_name' => 'Mamiek',
            'no_rekening' => '565466',
            'kontak_darurat' => '085657567567567'

        ]);
        // End Kondektur

        // sopir
        Employee::create([
            'employee_id' => '2024-EMP-02',
            'id_fingerprint' => '003',
            'employee_name' => 'Andi',
            'id_department' => 5,
            'position_id' => 16,
            'employee_status' => 'Tetap',
            'awal_kontrak' => '2023-03-24',
            'selesai_kontrak' => '2024-12-12',
            'jenis_kelamin' => 'L',
            'tanggal_lahir' => '1999-03-02',
            'status_perkawinan' => 'Belum Kawin',
            'alamat' => 'Yogyakarta',
            'alamat_domisili' => 'Yogyakarta',
            'nik' => '3525626547585685',
            'npwp' => '546546546546',
            'bpjs_kesehatan' => '6654645654636',
            'bpjs_ketenagakerjaan' => '5464565',
            'telepon' => '08546346346',
            'email' => 'andimail@gmail.com',
            'rekening_name' => 'Andi',
            'no_rekening' => '565466',
            'kontak_darurat' => '085657567567567'

        ]);

        // kernet
        Employee::create([
            'employee_id' => '2024-EMP-07',
            'id_fingerprint' => '007',
            'employee_name' => 'Sani',
            'id_department' => 6,
            'position_id' => 18,
            'employee_status' => 'Tetap',
            'awal_kontrak' => '2023-03-24',
            'selesai_kontrak' => '2024-12-12',
            'jenis_kelamin' => 'L',
            'tanggal_lahir' => '1999-03-02',
            'status_perkawinan' => 'Belum Kawin',
            'alamat' => 'Yogyakarta',
            'alamat_domisili' => 'Yogyakarta',
            'nik' => '352562456547585685',
            'npwp' => '54654456546546',
            'bpjs_kesehatan' => '665465445654636',
            'bpjs_ketenagakerjaan' => '546445565',
            'telepon' => '0854456346346',
            'email' => 'sanimail@gmail.com',
            'rekening_name' => 'Sani',
            'no_rekening' => '5654662',
            'kontak_darurat' => '08562157567567567'

        ]);
    }
}
