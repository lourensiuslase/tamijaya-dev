<?php

use App\Models\MasterData\ProsedurBagian;
use Illuminate\Database\Seeder;

class ProsedurBagianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Ketebalan Kampas Rem'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Tromol'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Minyak Rem'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Tekanan Rem'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Tekanan Angin'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 1, 
            'prosedur_bagian_name' => 'Sistem Hand'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 2, 
            'prosedur_bagian_name' => 'Power Stir'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 2, 
            'prosedur_bagian_name' => 'Terot'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 2, 
            'prosedur_bagian_name' => 'Draklink'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 2, 
            'prosedur_bagian_name' => 'Minyak Power Stering'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Lampu Utama'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Lampu Senja'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Lampu Rem'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Lampu Kabin'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Lampu Bagasi'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 3, 
            'prosedur_bagian_name' => 'Sein'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 4, 
            'prosedur_bagian_name' => 'Ketebalan Kampas Kopling'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 4, 
            'prosedur_bagian_name' => 'Speling Pedal Kopling'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 4, 
            'prosedur_bagian_name' => 'Minyak Kopling'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 5, 
            'prosedur_bagian_name' => 'Ganti Oli Mesin setiap 15.000 KM'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 6, 
            'prosedur_bagian_name' => 'Ganti Oli Perneling setiap 45.000 KM'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 7, 
            'prosedur_bagian_name' => 'Ganti Oli Gardan setiap 45.000 KM'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 8, 
            'prosedur_bagian_name' => 'Ganti Filter Oli setiap 15.000 KM'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 9, 
            'prosedur_bagian_name' => 'Ganti Filter Solar setiap 10.000 KM'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 10, 
            'prosedur_bagian_name' => 'Spul'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 10, 
            'prosedur_bagian_name' => 'Plat'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 11, 
            'prosedur_bagian_name' => 'Knalpot'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 11, 
            'prosedur_bagian_name' => 'Pastikan tidak ada kebocoran'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 12, 
            'prosedur_bagian_name' => 'Kondisi Angin'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 12, 
            'prosedur_bagian_name' => 'Ketebalan Ban'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 12, 
            'prosedur_bagian_name' => 'Kondisi Pelek & Pentil'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 13, 
            'prosedur_bagian_name' => 'Volume Air Radiator'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 13, 
            'prosedur_bagian_name' => 'Sirip - sirip Radiator'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 13, 
            'prosedur_bagian_name' => 'Kondisi Accu'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 13, 
            'prosedur_bagian_name' => 'Volume Air Accu'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 14, 
            'prosedur_bagian_name' => 'V-Belt'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 14, 
            'prosedur_bagian_name' => 'Suara Mesin'
        ]);
        ProsedurBagian::create([
            'id_bagian_cek' => 14, 
            'prosedur_bagian_name' => 'Kebocoran Oli pada Mesin'
        ]);
    }
}
