<?php

use Illuminate\Database\Seeder;
use App\Models\MasterData\Office;

class OfficeSeeder extends Seeder
{
    public function run()
    {
        Office::create([
            'office_code' => 'OFC-001',
            'office_origin' => 'YOGYAKARTA',
            'office_name' => 'KANTOR GARASI',
            'office_address' => 'Jl. Tino Sidin, Cobongan, Ngestiharjo, Kec. Kasihan, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55184',
            'office_phone' => '(0274) 618922,Kantor Pusat | (0274) 618967,Kantor Cabang | +62 811-250-136,Ike | +62 811-250-147, Joko | +62 822-268-80162,Dwi | +62 813-2834-3429,Antok | +62 857-2563-6078,Angel | +62 857-0028-6955,Okti'
        ]);
        Office::create([
            'office_code' => 'OFC-002',
            'office_origin' => 'BALI',
            'office_name' => 'KANTOR GARASI',
            'office_address' => 'Jl. Bulu Indah - Gg. Pondok Indah, Denpasar, Bali',
            'office_phone' => ' (0361) 9076845,Kantor Pusat | +62 813-3865-2996,Wayan | +62 878-6212-1955,Putu | +62 851-0095-7592,Ayu | +62 812-3674-6978,Komang'
        ]);
    }
}
