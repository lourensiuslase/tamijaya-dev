<?php

use Illuminate\Database\Seeder;
use App\Models\MasterData\Bahan_bakar;

class BahanBakarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bahan_bakar::create([
            'status' => 'PENUH',
            'deskripsi' => 'Indikator Lebih Dari 75%',
        ]);
        Bahan_bakar::create([
            'status' => 'SETENGAH',
            'deskripsi' => 'Indikator Antara 50% - 75%',
        ]);
        Bahan_bakar::create([
            'status' => 'HABIS',
            'deskripsi' => 'Indikator Kurang Dari 50%',
        ]);
    }
}
