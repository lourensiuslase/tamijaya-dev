<?php

use App\Models\MasterData\BagianCek;
use Illuminate\Database\Seeder;

class BagianCekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BagianCek::create([
            'name' => 'Sistem Pengereman dan Hand Rem'
        ]);
        BagianCek::create([
            'name' => 'Roda dan Kemudi'
        ]);
        BagianCek::create([
            'name' => 'Sistem Kelistrikan'
        ]);
        BagianCek::create([
            'name' => 'Kampas Kopling'
        ]);
        BagianCek::create([
            'name' => 'Oli Mesin'
        ]);
        BagianCek::create([
            'name' => 'Oli Perneling'
        ]);
        BagianCek::create([
            'name' => 'Oli Gardan'
        ]);
        BagianCek::create([
            'name' => 'Filter Oli'
        ]);
        BagianCek::create([
            'name' => 'Filter Solar'
        ]);
        BagianCek::create([
            'name' => 'Klakson'
        ]);
        BagianCek::create([
            'name' => 'Ketebalan Asap/Gas Buang'
        ]);
        BagianCek::create([
            'name' => 'Ban'
        ]);
        BagianCek::create([
            'name' => 'Radiator & Accu'
        ]);
        BagianCek::create([
            'name' => 'Mesin'
        ]);
    }
}
