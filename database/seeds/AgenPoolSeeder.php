<?php

use Illuminate\Database\Seeder;
use App\Models\MasterData\AgenPool;

class AgenPoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AgenPool::create([
            'name' => 'AGEN DONGKELAN', 
            'lokasi' => 'Jl. Ringroad Selatan, Dongkelan, Panggungharjo, Kec. Sewon, Kabupaten Bantul, Daerah Istimewa Yogyakarta 55188', 
            'number_phone' => '628101419935',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'YOGYAKARTA', 
            'link_maps'=> 'https://maps.app.goo.gl/c3vHNKqrstsdxuCm7'
        ]);
        AgenPool::create([
            'name' => 'AGEN DUL', 
            'lokasi' => 'Jl. Nasional III, Ruko 1, Ringroud Selatan, Giwangan, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55163', 
            'number_phone' => '6281329119041',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'YOGYAKARTA', 
            'link_maps'=> 'https://maps.app.goo.gl/gNdUNJuNweR4QETr7'
        ]);
        AgenPool::create([
            'name' => 'AGEN PRIMA', 
            'lokasi' => 'Kranggan, Bokoharjo, Kec. Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55572', 
            'number_phone' => '6282225995785',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'YOGYAKARTA', 
            'link_maps'=> 'https://maps.app.goo.gl/ZiygHNqj9pamY7Wn9'
        ]);
        AgenPool::create([
            'name' => 'AGEN IMAM KLATEN', 
            'lokasi' => 'Tengahan, Buntalan, Kec. Klaten Tengah, Kabupaten Klaten, Jawa Tengah, Indonesia', 
            'number_phone' => '6281393888907',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'KLATEN', 
            'link_maps'=> 'https://maps.app.goo.gl/gFq2xPrErnp3GAUB7'
        ]);
        AgenPool::create([
            'name' => 'OBY KLATEN', 
            'lokasi' => 'Tengahan, Buntalan, Kec. Klaten Tengah, Kabupaten Klaten, Jawa Tengah 57419', 
            'number_phone' => '6282211830972',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'KLATEN', 
            'link_maps'=> 'https://maps.app.goo.gl/gFq2xPrErnp3GAUB7'
        ]);
        AgenPool::create([
            'name' => 'MARKUS KARANGWUNI', 
            'lokasi' => 'Dlimas, Kec. Ceper, Kabupaten Klaten, Jawa Tengah 57465', 
            'number_phone' => '6281393117337',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'KLATEN', 
            'link_maps'=> 'https://maps.app.goo.gl/72BMpX6adUrtfiHB6'
        ]);
        AgenPool::create([
            'name' => 'ERLITA SOLO', 
            'lokasi' => 'Jl. Dr. Setiabudi, Gilingan, Kec. Banjarsari, Kota Surakarta, Jawa Tengah 57134', 
            'number_phone' => '6281237975405',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'SOLO', 
            'link_maps'=> 'https://maps.app.goo.gl/oUqZd23osqrzbumP6'
        ]);
        AgenPool::create([
            'name' => 'AGEN WISATA KOMODO', 
            'lokasi' => 'Jl. A. Yani No.262, Manahan, Kec. Banjarsari, Kota Surakarta, Jawa Tengah 57134', 
            'number_phone' => '6285868964366',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'SOLO', 
            'link_maps'=> 'https://maps.app.goo.gl/drjaCELuHX9FpGLS6'
        ]);
        AgenPool::create([
            'name' => 'AGEN BOGOWONTO SOLO', 
            'lokasi' => 'Jl. Dr. Setiabudi No.18B, Gilingan, Kec. Banjarsari, Kota Surakarta, Jawa Tengah 57134', 
            'number_phone' => '628926886703',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'SOLO', 
            'link_maps'=> 'https://maps.app.goo.gl/AE4q23uvm8zbwmBh8'
        ]);
        AgenPool::create([
            'name' => 'AGEN ADVEN', 
            'lokasi' => 'Terminal Tipe A Mengwi (Gd.A) Jl. Raya, Mengwitani, Kec. Mengwi, Kabupaten Badung, 80351', 
            'number_phone' => '6282146559008',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'BALI', 
            'link_maps'=> 'https://maps.app.goo.gl/KQoRptsKfSQ5x9TM7'
        ]);
        AgenPool::create([
            'name' => 'AGEN AYU', 
            'lokasi' => 'Terminal Tipe A Mengwi (Gd.A) Jl. Raya, Mengwitani, Kec. Mengwi, Kabupaten Badung, 80351', 
            'number_phone' => '6285100957592',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'BALI', 
            'link_maps'=> 'https://maps.app.goo.gl/KQoRptsKfSQ5x9TM7'
        ]);
        AgenPool::create([
            'name' => 'AGEN PUTU', 
            'lokasi' => 'Denpasar, Pemecutan Kaja, Kec. Denpasar Utara, Kota Denpasar, Bali 80111', 
            'number_phone' => '6287862121955',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'BALI', 
            'link_maps'=> 'https://maps.app.goo.gl/E5gkHhHoJgZ68yW26'
        ]);
        AgenPool::create([
            'name' => 'AGEN WAYAN', 
            'lokasi' => 'Denpasar, Pemecutan Kaja, Kec. Denpasar Utara, Kota Denpasar, Bali 80111', 
            'number_phone' => '6281338652996',
            'deskripsi' => 'JAM BUKA AGEN: 07.00 - 22.00', 
            'area' => 'BALI', 
            'link_maps'=> 'https://maps.app.goo.gl/E5gkHhHoJgZ68yW26'
        ]);
    }
}
