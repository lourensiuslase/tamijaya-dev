<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlagBengkelLuarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flag_bengkel_luar', function (Blueprint $table) {
            $table->id();
            $table->integer('id_armada');
            $table->string('nama');
            $table->integer('flag_bengkel_luar');
            $table->string('estimasi_harga')->nullable();
            $table->string('estimasi_selesai')->nullable();
            $table->string('upload_pdf')->nullable();

            $table->timestamp('tgl_ajukan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flag_bengkel_luar');
    }
}
