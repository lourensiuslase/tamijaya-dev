<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanSparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_sparepart', function (Blueprint $table) {
            $table->id();
            $table->integer('id_armada');
            $table->integer('id_komponen');
            $table->string('pic_pemohon');
            $table->integer('jml_permintaan');
            $table->string('deskripsi')->nullable();
            $table->integer('status_pengajuan_sparepart')->nullable();
            $table->date('tgl_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_sparepart');
    }
}
