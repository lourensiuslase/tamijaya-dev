<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBengkelDalamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bengkel_dalam', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cek_armada');
            $table->string('svp_checker');
            $table->integer('status_bengkel')->nullable();

            $table->time('tgl_masuk')->nullable();
            $table->time('tgl_keluar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bengkel_dalam');
    }
}
