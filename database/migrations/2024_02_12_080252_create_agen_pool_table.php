<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenPoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agen_pool', function (Blueprint $table) {
            $table->id();
            // $table->string('image');
            $table->string('name');
            $table->longText('lokasi');
            $table->string('number_phone');
            $table->longText('deskripsi');
            $table->string('area');
            $table->string('link_maps');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agen_pool');
    }
}
