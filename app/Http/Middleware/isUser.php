<?php

namespace App\Http\Middleware;

use App\Models\MasterData\Customer;
use Closure;
use Illuminate\Support\Facades\Auth;

class isUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('customer')->check()) {
            return abort(403);
        }
        return $next($request);
    }
}
