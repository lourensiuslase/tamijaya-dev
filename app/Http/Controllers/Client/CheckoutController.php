<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\General\Notification;
use App\Models\MasterData\Pasengger;
use App\Models\Transaction\BookingPayment;
use App\Models\Transaction\BookingSeat;
use App\Models\Transaction\BookingTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function transfer(Request $request)
    {
        $code_tr = 'CODE-' . mt_rand(000000000, 999999999);
        $cs = Auth::guard('customer')->user();
        $code = $cs->id;
        $name = $cs->customer_name;
        $item = $request->all();
        // dd($item);

        $validatedData = [
            'booking_transactions_code' => $code_tr,
            'booking_transactions_customer_code' => $code,
            'booking_transactions_type_booking' => 'REGULER',
            'booking_transactions_schedule_id' => $item["booking_transactions_schedule_id"],
            'booking_transactions_pick_point' => $item["booking_transactions_pick_point"],
            'booking_transactions_arrival_point' => $item["booking_transactions_arrival_point"],
            'booking_transactions_province_from' => NULL,
            'booking_transactions_city_from' => $cs->id_city !== NULL ? $cs->id_city : $item["booking_transactions_pick_point"],
            'booking_transactions_province_to' => NULL,
            'booking_transactions_city_to' => $cs->id_city !== NULL ? $cs->id_city : $item["booking_transactions_arrival_point"],
            'booking_transactions_total_seats' => $item["booking_transactions_total_seats"],
            'booking_transactions_type_down_payment' => $item["booking_transactions_type_down_payment"],
            'booking_transactions_payment_method' => $item["booking_transactions_payment"],
            'booking_transactions_total_costs' => $item["booking_transactions_total_costs"],
            'booking_transactions_total_discount' => NULL,
            'booking_transactions_additional_price' => NULL,
            'booking_transactions_total_down_payment' => $item["booking_transactions_total_down_payment"],
            'booking_transactions_outstanding_payment' => NULL,
            'booking_transactions_id_payment' => $item["booking_transactions_id_payment"],
            'booking_transactions_payment_attachment' => NULL,
            'booking_transactions_is_agent' => NULL,
            'booking_transactions_id_agent' => NULL,
            'booking_transactions_status' => 7,
            'booking_transactions_reschedule_date' => $item["booking_transactions_reschedule_date"],
            'created_by' => $name,
            'finance_by' => NULL,
            'updated_by' => NULL,
        ];
        if($request->file()){
            $validatedData['booking_transactions_pic'] = $request->file('booking_transactions_transactions_pic')->store('transaction_pic');
        } else {
            $validatedData['booking_transactions_pic'] = NULL;
        }
        

        $data = BookingTransaction::create($validatedData);

        // pasengger
        $pasengger1 = [
            'customer_id' => $cs->id, 
            'name' => $cs->customer_name, 
            'nik' => $cs->customer_nik ? $cs->customer_nik : 'NULL'       
        ];
        Pasengger::create($pasengger1);
        if($item["name"]) {
            $pasengger = [
                'customer_id' => $cs->id, 
                'name' => $item["name"], 
                'nik' => $item["nik"]        
            ];
            Pasengger::create($pasengger);
        }

        $data_payment = [
            'id_booking_transaction' => $data->id,
            'amount' => $data->booking_transactions_total_down_payment,
            'payment_type' => $data->booking_transactions_payment_method,
            'status' => 'PAID',
            'attachment' => $data->booking_transactions_transactions_pic !== NULL ? $data->booking_transactions_transactions_pic : NULL,
            'send_by' => $cs->id,
            'approved_by' => NULL
        ];
        BookingPayment::create($data_payment);

        $bookedSeat = $item['travel_selected_seat'];
        $costSeat = $item['booking_transactions_total_costs'];
        $seat_code = 'SEATS -' . mt_rand(0000, 9999);
        $booking_seat = [
            'booking_seats_schedule_id' => $item['booking_transactions_schedule_id'],
            'booking_seats_booking_id' => $data['id'],
            'booking_seats_seat_number' => $bookedSeat,
            'booking_seats_seat_price' => $costSeat,
            'booking_seats_code' => $seat_code
        ];
        BookingSeat::create($booking_seat);
        

        return redirect('/')->with('success', 'Silahkan tunggu validasi dari admin'); //#mengarah ke transaction success

    }
}
