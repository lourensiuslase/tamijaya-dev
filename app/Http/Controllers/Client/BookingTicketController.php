<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\MasterData\Bank;
use App\Models\MasterData\Customer;
use App\Models\MasterData\PickPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingTicketController extends Controller
{
    public function index() {
        return view('client.booking.ticket.main');
    }

    public function detailBus(Request $request,$id)
    {
        $availableBuses = DB::table('schedule_regulers')
                            ->select('schedule_regulers.id', 'schedule_regulers.*', 'armadas.armada_type', 'armadas.armada_no_police', 'armadas.armada_seat', 'armadas.*', 'pick_points.pick_point_origin', 'pick_point_name', 'pick_points.*')
                            ->join('armadas', 'schedule_regulers.id_armada', '=', 'armadas.ID')
                            ->leftjoin('pick_points', 'armadas.id_pick_point', '=', 'pick_points.id')
                            ->where('schedule_regulers.id', $id)
                            ->get();
        // dd($availableBuses);
        $pickpoint = PickPoint::all();
        return view('client.booking.ticket.detail', [
            'detailBus' => $availableBuses, 
            'customers' => Customer::latest(), 
            'pickpoints' => $pickpoint, 
        ]);
    }

    public function cekTiket(Request $request) 
    {
        $destination = $request->detail_tujuan;
        $departureDate = $request->detail_date_departure;
    
        $availableBuses = DB::table('schedule_regulers')
                            ->select('schedule_regulers.id', 'schedule_regulers.destination', 'armadas.armada_type', 'armadas.armada_no_police', 'armadas.armada_seat', 'armadas.*', 'schedule_regulers.*')
                            ->join('armadas', 'schedule_regulers.id_armada', '=', 'armadas.ID')
                            ->where('schedule_regulers.date_departure', $departureDate)
                            ->where('schedule_regulers.destination', $destination)
                            ->get();
        if ($availableBuses->isEmpty()) {
            return response()->json([
                'message' => 'Tidak ada bus tersedia untuk tujuan dan tanggal tersebut',
                'status' => 'error'
            ], 404);
        }
    
        return response()->json([
            'message' => 'Berikut adalah bus yang tersedia',
            'status' => 'success',
            'data' => $availableBuses
        ]);
    }

    public function getPickPoint()
    {
        $pickPointJogja = DB::select("SELECT id, concat(pick_point_origin, ' = ', pick_point_name) `text` 
                                FROM `pick_points` 
                                WHERE pick_point_origin = 'JOGJA' OR pick_point_origin = 'KLATEN-SOLO-NGAWI'");
        $pickPointDenpasar = DB::select("SELECT id, concat(pick_point_origin, ' = ', pick_point_name) `text`
                                FROM `pick_points`
                                WHERE pick_point_origin = 'DENPASAR'");

        return response()->json([
            'JOGJA' => $pickPointJogja, 
            'DENPASAR' => $pickPointDenpasar
        ]);
    }

    public function payment(Request $request)
    {
        $data = $request->all();
        // dd($data);
        return view('client.booking.ticket.payment', [
            'banks' => Bank::all(), 
            'total_price' => $data['travel_detail_total_cost'], 
            'data' => $data
        ]);
    }

    public function bank($id)
    {
        $data = Bank::findOrFail($id);
        return response()->json([
            'data' => $data,
            'status' => $data ? 200 : 400,
        ]);
    }

    // public function getSeatPublik(Request $request)
    // {
    //     if ($request->typeTravel == 'SUITESS') {
    //         return view('admin.transaction.reguler.seat.suitess');
    //     } else {
    //         return view('admin.transaction.reguler.seat.executive');
    //     }
    // }
}
