<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\MasterData\Bahan_bakar;


class BahanBakarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master-data.bbm.index', [
            'items' => Bahan_bakar::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
    
        $item = new Bahan_bakar;
        $item->status = $request->status;
        $item->deskripsi = $request->deskripsi;
        $item->save();

        DB::commit();
            Session::flash('message', ['Berhasil menyimpan data bahan bakar', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal menyimpan data bahan bakar', 'error']);
        }

        return redirect()->route('bahanbakar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
    
        $item = Bahan_bakar::findOrFail($id);
        $item->status = $request->status;
        $item->deskripsi = $request->deskripsi;
        $item->save();

        DB::commit();
            Session::flash('message', ['Berhasil mengubah data bahan bakar', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal mengubah data bahan bakar', 'error']);
        }

        return redirect()->route('bahanbakar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bahan_bakar::destroy($id);
        return response()->json([
            'data' => $id,
            'message' => 'Berhasil menghapus data bahan bakar',
            'status' => 200,
        ]);
    }
}
