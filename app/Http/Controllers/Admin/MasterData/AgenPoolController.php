<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use App\Models\MasterData\AgenPool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AgenPoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master-data.agen-pool.index', [
            'items' => AgenPool::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $item = $request->all();
            $item['number_phone'] = '62' . $request->number_phone;

            $data = AgenPool::create($item);

            DB::commit();
            Session::flash('message', ['Berhasil menyimpan data agen pool', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal menyimpan data agen pool', 'error']);
        }

        return redirect()->route('agen-pool.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $item = AgenPool::findOrFail($id);

            $item->update($data);

            DB::commit();
            Session::flash('message', ['Berhasil mengubah data agen pool', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal mengubah data agen pool', 'error']);
        }

        return redirect()->route('agen-pool.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AgenPool::destroy($id);
        return response()->json([
            'data' => $id, 
            'message' => 'Berhasil menghapus data agen pool', 
            'status' => 200
        ]);
    }

    public function getAgenPool($code)
    {
        $data = AgenPool::orderBy('name', 'ASC')->where('area', $code)->get();

        return response()->json([
            'success' => true, 
            'data' => $data, 
            'message' => 'Berhasil mendapatkan data agen pool', 
            'status' => 200
        ]);
    }
}
