<?php

namespace App\Http\Controllers\Admin\MarketingTicketing\PemanduPerjalanan\ChecklistPenumpang;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SuitesController extends Controller
{
    public function index()
    {
        $data = collect(DB::select(
            "SELECT bt.id
             , bs.booking_seats_seat_number as no_kursi
             , c.customer_name AS nama_penumpang
             , bt.booking_transactions_code AS kode_booking
             , bp.status AS status_pembelian
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_pick_point) AS pick_point
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_arrival_point) AS arrival_point
             ,pp.pick_point_eta AS jam
             ,sr.type_bus AS tipe
             ,a.armada_no_police AS nopol
             ,sr.driver_1
             ,sr.driver_2
             ,sr.conductor

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             JOIN `armadas` a ON a.`id_pick_point` = pp.id
             WHERE bp.status LIKE '%PAID'
             "));


        $total = collect(DB::select(
            "SELECT count(bt.id) as total

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             JOIN `armadas` a ON a.`id_pick_point` = pp.id
             WHERE bp.status LIKE '%PAID'
             "));
        //dd($data);
        return view('admin.marketing-ticketing.pemandu-perjalanan.checklist-penumpang.suites.index',['data'=>$data,'total'=>$total]);
    }
}
