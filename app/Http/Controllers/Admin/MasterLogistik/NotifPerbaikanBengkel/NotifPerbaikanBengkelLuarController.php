<?php

namespace App\Http\Controllers\Admin\MasterLogistik\NotifPerbaikanBengkel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NotifPerbaikanBengkelLuarController extends Controller
{
    public function index()
    {
        $pengajuanBengkelLuar = DB::select("SELECT fbl.id, fbl.id_armada,pk.id_prosedur_bagian,fbl.flag_bengkel_luar, a.armada_no_police, bc.name AS name_bagian, pb.prosedur_bagian_name, fbl.nama AS name_bengkel_luar, fbl.tgl_ajukan, fbl.estimasi_harga, fbl.estimasi_selesai  FROM `flag_bengkel_luar` fbl
                                            JOIN armadas a ON fbl.id_armada = a.id 
                                            LEFT JOIN pengajuan_keluhan pk ON a.id = pk.id_armada
                                            LEFT JOIN prosedur_bagian pb ON pk.id_prosedur_bagian = pb.id
                                            LEFT JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                            ");
        $detailKerusakan = DB::select("SELECT * FROM `pengajuan_keluhan` pk
                                        JOIN armadas a ON pk.id_armada = a.id 
                                        JOIN prosedur_bagian pb ON pk.id_prosedur_bagian = pb.id
                                        LEFT JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                        -- WHERE pk.id_armada = a.id && pk.id_prosedur_bagian = pb.id
                                    ");
        // $detailKerusakan = DB::select('SELECT * FROM `prosedur_bagian` pb 
        //                                 JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
        //                                 JOIN pengajuan_keluhan pk ON pb.id = pk.id_prosedur_bagian
        //                                 LEFT JOIN armadas a ON pk.id_armada = a.id'
        //                                 );
                                            // dd($pengajuanBengkelLuar);
        return view('admin.master-logistik.notif-perbaikan-bengkel-luar.index', compact('pengajuanBengkelLuar', 'detailKerusakan'));
    }
}
