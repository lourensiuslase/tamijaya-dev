<?php

namespace App\Http\Controllers\Admin\MasterLogistik\NotifPermintaan;

use App\Http\Controllers\Controller;
use App\Models\MasterDataLogistik\PengajuanSparepart;
use Illuminate\Support\Facades\DB;

class NotifPermintaanLogistikController extends Controller
{
    public function index()
    {
        $dataPengajuan = DB::select('SELECT ps.id, ps.pic_pemohon, ps.jml_permintaan, ps.status_pengajuan_sparepart, a.armada_no_police, k.nama_komponen, k.stok, sb.nama_sub_bagian, b.nama_bagian FROM `pengajuan_sparepart` ps
                                    JOIN armadas a ON ps.id_armada = a.id 
                                    JOIN komponens k ON ps.id_komponen = k.id
                                    LEFT JOIN sub_bagians sb ON k.sub_bagian_id = sb.id
                                    LEFT JOIN bagians b ON sb.bagian_id = b.id
                                    ');
                                    // dd($dataPengajuan);
        return view('admin.master-logistik.notif-permintaan-logistik.index', compact('dataPengajuan'));
    }

    public function setujuiSparepart($id)
    {
        $item = PengajuanSparepart::findOrFail($id);
        $item->status_pengajuan_sparepart = 1;
        $item->save();

        return redirect()->back();
    }

    public function tolakSparepart($id)
    {
        $item = PengajuanSparepart::findOrFail($id);
        $item->status_pengajuan_sparepart = 2;
        $item->save();

        return redirect()->back();
    }
}
