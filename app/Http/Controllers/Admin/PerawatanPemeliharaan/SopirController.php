<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan;

use App\Http\Controllers\Controller;
use App\Models\MasterData\Armada;
use App\Models\MasterDataLogistik\Bagian;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SopirController extends Controller
{
    public function listCheckFisik()
    {
        $data = collect(DB::select(
            "SELECT bt.id
             , bs.booking_seats_seat_number as no_kursi
             , c.customer_name AS nama_penumpang
             , bt.booking_transactions_code AS kode_booking
             , bp.status AS status_pembelian
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_pick_point) AS pick_point
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_arrival_point) AS arrival_point
             ,pp.pick_point_eta AS jam
             ,sr.type_bus AS tipe
             ,a.armada_no_police AS nopol
             ,sr.driver_1
             ,sr.driver_2
             ,sr.conductor

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             LEFT JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             LEFT JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             LEFT JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             LEFT JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             LEFT JOIN `armadas` a ON a.`id_pick_point` = pp.id
             WHERE bp.status LIKE '%PAID'
             "));
        $armada = Armada::get();
        $bagian = Bagian::get();
        $sopir = CekLayananFisik::select("check_fisik_layanan.*", 'bagians.nama_bagian as bagian', 'armadas.armada_merk as merk')
            ->join('bagians', 'bagians.id', '=', 'check_fisik_layanan.bagian_id')
            ->join('armadas', 'armadas.id', '=', 'check_fisik_layanan.id_armada')
            ->orderBy('bagians.id')
            ->get();
        return view('admin.perawatan-pemeliharaan.sopir.check-fisik-layanan',  ['armada' => $armada,'bagian'=>$bagian,'sopir'=>$sopir,'data'=>$data]);
    }


    public function SumpanCheckList(Request $request)
    {

    }

    public function ReportPerjalanan()
    {
        return view('admin.perawatan-pemeliharaan.sopir.report-perjalanan');
    }



}
