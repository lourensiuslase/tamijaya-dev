<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan;

use App\Http\Controllers\Controller;
use App\Models\PerawatanPemeliharaan\Bengkel\FlagBengkelLuar;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use App\Models\PerawatanPemeliharaan\PetugasCek\CekArmada;
use App\Models\PerawatanPemeliharaan\PetugasCek\FlagKeluhan;
use App\Models\PerawatanPemeliharaan\PetugasCek\PengajuanKeluhan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SupervisorCheckArmadaController extends Controller
{
    public function listApprovalSparepart()
    {
//        $currentDate = Carbon::now()->toDateString();
//        $CekArmada = CekArmada::select("cuci_armadas.*", 'check_fisik_layanan.*', 'armadas.*', 'bagians.*')
//            ->join('check_fisik_layanan', 'cuci_armadas.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
//            ->Leftjoin('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
//            ->Leftjoin('bagians', 'check_fisik_layanan.bagian_id', '=', 'bagians.id')
//            ->whereNotIn('bagians.id', [2])
//            ->get();
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.approval-sparepart.list');
    }

    public function listApprovalLogistik()
    {
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.approval-logistik-perjalanan.list');
    }

    public function listPenentuanBengkel()
    {
<<<<<<< HEAD
        $SpvCheck = DB::select('SELECT ca.id, ca.check_fisik_layanan_id, a.armada_no_police,cfl.id_armada, cfl.status_cuci, cfl.status_cek, bd.id_cek_armada, bd.status_bengkel, fbl.flag_bengkel_luar FROM `cek_armada` ca
                        JOIN check_fisik_layanan cfl ON ca.check_fisik_layanan_id = cfl.id
                        JOIN armadas a ON cfl.id_armada = a.id 
                        LEFT JOIN flag_bengkel_luar fbl ON a.id = fbl.id_armada
                        LEFT JOIN bengkel_dalam bd ON ca.id = bd.id_cek_armada
                        ');
                        // dd($SpvCheck);
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.penentuan-bengkel-luar-dalam.list', 
            compact('SpvCheck')
        );
=======
//        $SpvCheck = DB::select('SELECT ca.id, ca.check_fisik_layanan_id, a.armada_no_police,cfl.id_armada, cfl.status_cuci, cfl.status_cek, bd.id_cek_armada, bd.status_bengkel FROM `cek_armada` ca
//                        JOIN check_fisik_layanan cfl ON ca.check_fisik_layanan_id = cfl.id
//                        JOIN armadas a ON cfl.id_armada = a.id
//                        LEFT JOIN bengkel_dalam bd ON ca.id = bd.id_cek_armada
//                        ');
        // dd($SpvCheck);
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.penentuan-bengkel-luar-dalam.list');
>>>>>>> 4788314ec9447449139ce18cd0e3877ad9a240f4
    }

    public function detailPengajuanSupervisor($id)
    {
        $dataPengajuanKeluhan = DB::select('SELECT pb.*, bc.name, pk.id FROM `prosedur_bagian` pb
                                            JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                            JOIN pengajuan_keluhan pk ON pb.id = pk.id_prosedur_bagian');
        $data = CekArmada::select("cek_armada.*", "check_fisik_layanan.*", "armadas.armada_no_police")->join('check_fisik_layanan', 'cek_armada.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
            ->join('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
            ->where('check_fisik_layanan.id_armada', $id)
            ->get();
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.penentuan-bengkel-luar-dalam.detail', [
            'detailPengajuanMontir' => $dataPengajuanKeluhan,
            'data' => $data,
            'id_armada' => $id,
        ]);
    }

    public function clearPengajuanSupervisor($id)
    {
        $item = PengajuanKeluhan::findOrFail($id);
        $item->delete();

        return redirect()->back();
    }

    public function approvalMontir($id)
    {
        $item = CekLayananFisik::findOrFail($id);
        $item->status_cek = 3;
        $item->save();

        return redirect()->route('perawatan-pemeliharaan.supervisor-check-armada.list-penentuan-bengkel');
    }

    public function approvalSVP($id)
    {
        $item = CekLayananFisik::findOrFail($id);
        $item->status_cek = 1;
        $item->save();

        return redirect()->route('perawatan-pemeliharaan.supervisor-check-armada.list-penentuan-bengkel');
    }

    public function pengajuanBengkelLuar(Request $request)
    {
        $item = $request->all();
        $data = [
            'id_armada' => $item["id_armada"],
            'nama' => $item['nama'],
            'estimasi_harga' => $item['estimasi_harga'],
            'estimasi_selesai' => $item['estimasi_selesai'],
            'flag_bengkel_luar' => 1, 
            'tgl_ajukan' => now()
        ];
        FlagBengkelLuar::create($data);

        return redirect()->back();
    }

}
