<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\PetugasCuci;

use App\Http\Controllers\Controller;
use App\Models\MasterData\BagianCuciArmada;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use App\Models\PerawatanPemeliharaan\PetugasCek\CekArmada;
use App\Models\PerawatanPemeliharaan\PetugasCuci\CuciArmada;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CuciArmadaController extends Controller
{
    public function  listNotifikasi()
    {

        $currentDate = Carbon::now()->toDateString();
        // $a = CuciArmada::select("cuci_armadas.*", 'check_fisik_layanan.*', 'armadas.*', 'bagians.*', 'bahan_bakar.*', 'bagian_cuci_armada.*')
        //     ->join('check_fisik_layanan', 'cuci_armadas.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
        //     ->Leftjoin('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
        //     ->Leftjoin('bagians', 'check_fisik_layanan.bagian_id', '=', 'bagians.id')
        //     ->leftjoin('bahan_bakar', 'check_fisik_layanan.id_bbm', '=', 'bahan_bakar.id')
        //     ->leftjoin('bagian_cuci_armada', 'armadas.id', '=', 'bagian_cuci_armada.id_armada')
        //     ->where('check_fisik_layanan_id', '=', '5')
        //     ->get();
// dd($CuciArmada);
        $CuciArmada = DB::select("SELECT cfl.*, a.*, b.*, bb.*, ca.*, bca.* FROM `cek_armada` ca 
                                    JOIN check_fisik_layanan cfl ON ca.check_fisik_layanan_id = cfl.id
                                    LEFT JOIN armadas a ON cfl.id_armada = a.id 
                                    LEFT JOIN bagians b ON cfl.bagian_id = b.id 
                                    LEFT JOIN bahan_bakar bb ON cfl.id_bbm = bb.id
                                    LEFT JOIN bagian_cuci_armada bca ON a.id = bca.id_armada
                                    ");
        // dd($CuciArmada);
        $bagianArmada = DB::select("SELECT bca.id, bca.* FROM `bagian_cuci_armada` bca JOIN armadas a ON bca.id_armada = a.id");
        $bagianArmadaEksterior = DB::select("SELECT bca.*, a.id FROM `bagian_cuci_armada` bca JOIN armadas a ON bca.id_armada = a.id");
        // dd($bagianArmada);
        return view('admin.perawatan-pemeliharaan.petugas-cuci.list-notifikasi', compact('CuciArmada','currentDate', 'bagianArmada', 'bagianArmadaEksterior'));
    }

    public function ValidasiCuciArmada (Request $request)
    {
        DB::beginTransaction();

        try {
            foreach ($request->check_fisik_layanan_id as $check_fisik_layanan_id) {
                $sopir = new CekArmada();
                $sopir->check_fisik_layanan_id = $check_fisik_layanan_id;

//                dd($sopir);
                $sopir->save();
            }

            DB::commit();
            Session::flash('message', ['Berhasil mengajukan pengajuan pembelian', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal mengajukan pengajuan pembelian', 'error']);
        }

        return redirect()->route('perawatan-pemeliharaan.sopir.check-fisik-layanan');
    }



    public function setujuiCuci($id)
    {
        // $validasi = CekLayananFisik::where('id', $id)->update(['status_cuci' => 1]);
        $validasi = CekLayananFisik::findOrFail($id);
        $validasi->status_cuci = 1;
        $validasi->save();
        // dd($validasi);
        return redirect()->route('perawatan-pemeliharaan.petugas-cuci.list-notifikasi-cuci');
    }

    public function checklistEksterior($id)
    {
        $check = BagianCuciArmada::findOrFail($id);
        $check->flag_eksterior = 1;
        $check->save();

        return redirect()->route('perawatan-pemeliharaan.petugas-cuci.list-notifikasi-cuci');
    }

    public function checklistInterior($id)
    {
        $check = BagianCuciArmada::findOrFail($id);
        $check->flag_interior = 1;
        $check->save();

        return redirect()->route('perawatan-pemeliharaan.petugas-cuci.list-notifikasi-cuci');
    }
}
