<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan;

use App\Http\Controllers\Controller;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use App\Models\PerawatanPemeliharaan\PetugasCuci\CekArmada;
use App\Models\PerawatanPemeliharaan\PetugasCuci\CuciArmada;
use Carbon\Carbon;

class SupervisorCuciController extends Controller
{
    public function listApproval()
    {

        $currentDate = Carbon::now()->toDateString();
        $CuciArmada = CuciArmada::select("cuci_armadas.*", 'check_fisik_layanan.*', 'armadas.*', 'bagians.*')
            ->join('check_fisik_layanan', 'cuci_armadas.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
            ->Leftjoin('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
            ->Leftjoin('bagians', 'check_fisik_layanan.bagian_id', '=', 'bagians.id')
            ->where('bagians.id', [2])
            ->get();
        return view('admin.perawatan-pemeliharaan.supervisor-cuci-mobil.list-approval-laporan',compact('CuciArmada','currentDate'));
    }

    public function ReportCuci()
    {
        return view('admin.perawatan-pemeliharaan.supervisor-cuci-mobil.report-cuci');
    }
    public function setujuiCek($id)
    {
        CekLayananFisik::where('id', $id)->update(['spv_cek' => 1]);
        return redirect()->route('admin.perawatan-pemeliharaan.supervisor-cuci-mobil.report-cuci');
    }
}
