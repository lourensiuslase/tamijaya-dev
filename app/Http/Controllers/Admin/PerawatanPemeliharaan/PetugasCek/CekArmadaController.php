<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\PetugasCek;

use App\Http\Controllers\Controller;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use App\Models\PerawatanPemeliharaan\PetugasCek\CekArmada;
use App\Models\PerawatanPemeliharaan\PetugasCek\CuciArmada;
use App\Models\PerawatanPemeliharaan\PetugasCek\FlagKeluhan;
use App\Models\PerawatanPemeliharaan\PetugasCek\PengajuanKeluhan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CekArmadaController extends Controller
{
    public function  listNotifikasi()
    {

        $currentDate = Carbon::now()->toDateString();
        $CekArmada = DB::select("SELECT cfl.*, a.*, b.*, bb.*, ca.*, fk.flag FROM `cek_armada` ca 
                    JOIN check_fisik_layanan cfl ON ca.check_fisik_layanan_id = cfl.id
                    LEFT JOIN armadas a ON cfl.id_armada = a.id 
                    LEFT JOIN bagians b ON cfl.bagian_id = b.id 
                    LEFT JOIN bahan_bakar bb ON cfl.id_bbm = bb.id
                    LEFT JOIN flag_keluhan fk ON a.id = fk.id_armada
                    ");
        $PengajuanKeluhan = DB::select('SELECT pb.*, bc.*, pk.*, pk.deskripsi, pb.prosedur_bagian_name, cfl.keluhan, a.armada_no_police, bc.name FROM `pengajuan_keluhan` pk
                            JOIN prosedur_bagian pb ON pk.id_prosedur_bagian = pb.id 
                            JOIN check_fisik_layanan cfl ON pk.check_fisik_layanan_id = cfl.id 
                            JOIN armadas a ON pk.id_armada = a.id
                            LEFT JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id;');
    //   dd($PengajuanKeluhan);
        $dataProsedurBagian = DB::select('SELECT pb.*, bc.name FROM `prosedur_bagian` pb 
                                        JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                        ');
        // dd($dataProsedurBagian);
        return view('admin.perawatan-pemeliharaan.petugas-cek.list-notifikasi', compact('CekArmada','currentDate', 'PengajuanKeluhan', 'dataProsedurBagian'));
    }



    public function setujuiCek($id)
    {
        $item = CekLayananFisik::findOrFail($id);
        $item->status_cek = 2;
        $item->save();
        return redirect()->route('perawatan-pemeliharaan.petugas-cek.list-notifikasi-cek');
    }

    public function pengajuanKeluhan(Request $request)
    {
        $data = $request->all();
        $item = [
            'id_armada' => $data["id_armada"], 
            'id_prosedur_bagian' => $data["id_prosedur_bagian"], 
            'deskripsi' => $data["deskripsi"]
        ];
        pengajuanKeluhan::create($item);

        return redirect()->back();
    }

    public function detailPengajuanMontir($id) {
        $dataPengajuanKeluhan = DB::select('SELECT pb.*, bc.name, pk.id_armada FROM `prosedur_bagian` pb 
                                            JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                            LEFT JOIN pengajuan_keluhan pk ON pb.id = pk.id_prosedur_bagian
                                            LEFT JOIN armadas a ON pk.id_armada = a.id
                                            ');
        $data = CekArmada::select("cek_armada.*", "check_fisik_layanan.*", "armadas.armada_no_police")->join('check_fisik_layanan', 'cek_armada.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
                            ->join('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
                            ->where('check_fisik_layanan.id_armada', $id)
                            ->get();
        // $keluhan = pengajuanKeluhan::where('id_prosedur_bagian', $id)->get();
                            // dd($keluhan);
        return view('admin.perawatan-pemeliharaan.petugas-cek.detail-pengajuan', [
            'detailPengajuanMontir' => $dataPengajuanKeluhan, 
            'data' => $data, 
            'id_armada' => $id, 
            // 'keluhan' => $keluhan
        ]);
    }

}
