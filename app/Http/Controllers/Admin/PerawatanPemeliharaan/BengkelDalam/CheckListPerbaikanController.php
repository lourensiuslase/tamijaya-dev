<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\BengkelDalam;

use App\Http\Controllers\Controller;
use App\Models\PerawatanPemeliharaan\PetugasCek\CekArmada;
use App\Models\PerawatanPemeliharaan\PetugasCek\PengajuanKeluhan;
use Illuminate\Support\Facades\DB;

class CheckListPerbaikanController extends Controller
{
    public function checklistPerbaikan($id)
    {
        $detailPengajuanMontir = DB::select('SELECT pb.*, bc.name, pk.id FROM `prosedur_bagian` pb 
                                            JOIN bagian_cek bc ON pb.id_bagian_cek = bc.id
                                            JOIN pengajuan_keluhan pk ON pb.id = pk.id_prosedur_bagian');
        $data = CekArmada::select("cek_armada.*", "check_fisik_layanan.*", "armadas.armada_no_police")->join('check_fisik_layanan', 'cek_armada.check_fisik_layanan_id', '=', 'check_fisik_layanan.id')
                            ->join('armadas', 'check_fisik_layanan.id_armada', '=', 'armadas.id')
                            ->where('check_fisik_layanan.id_armada', $id)
                            ->get();
        $id = $id;
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.bengkel-dalam.checklist-perbaikan-bengkel', 
            compact('detailPengajuanMontir', 'data', 'id')
        );
    }

    public function deletePerbaikan($id)
    {
        $item = PengajuanKeluhan::findOrFail($id);
        $item->delete();

        return redirect()->back();
    }
}
