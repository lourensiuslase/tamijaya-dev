<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\BengkelDalam;

use App\Http\Controllers\Controller;
use App\Models\MasterData\Armada;
use App\Models\MasterDataLogistik\Komponen;
use App\Models\MasterDataLogistik\PengajuanSparepart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengajuanLogistikController extends Controller
{
    public function PengajuanLogistikDalam()
    {
        $dataArmada = Armada::all();
        $dataKomponen = Komponen::all();
        $auth = Auth()->user()->name;
        $pengajuan = DB::select("SELECT * FROM `pengajuan_sparepart` ps 
                                JOIN armadas a ON ps.id_armada = a.id
                                JOIN komponens k ON ps.id_komponen = k.id
                                LEFT JOIN sub_bagians sb ON k.sub_bagian_id = sb.id
                                WHERE ps.pic_pemohon = '$auth'
                                ");
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.bengkel-dalam.pengajuan-logistik', compact('dataArmada', 'dataKomponen', 'pengajuan'));
    }

    public function pengajuanSparepart(Request $request)
    {
        $item = $request->all();
        $data = [
            'id_armada' => $item["id_armada"],
             'pic_pemohon' => $item["pic_pemohon"], 
             'id_komponen' => $item["id_komponen"], 
             'jml_permintaan' => $item["jml_permintaan"],
             'deskripsi' => $item["deskripsi"],
             'tgl_order' => now()
        ];
        PengajuanSparepart::create($data);

        return redirect()->route('perawatan-pemeliharaan.bengkel-dalam.list-pengajuan-logistik');
    }
}
