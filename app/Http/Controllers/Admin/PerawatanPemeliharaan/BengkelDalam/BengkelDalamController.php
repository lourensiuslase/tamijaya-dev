<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\BengkelDalam;

use App\Http\Controllers\Controller;
use App\Models\PerawatanPemeliharaan\Bengkel\BengkelDalam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BengkelDalamController extends Controller
{
    public function listBengkelDalam()
    {
//        $dataBengkelDalam = DB::select('SELECT bd.*, cfl.id_armada , a.armada_no_police, a.armada_type, a.armada_category FROM `bengkel_dalam` bd
//                                        JOIN cek_armada ca ON bd.id_cek_armada = ca.id
//                                        LEFT JOIN check_fisik_layanan cfl ON ca.check_fisik_layanan_id = cfl.id
//                                        LEFT JOIN armadas a ON cfl.id_armada = a.id
//                                    ');
        // dd($dataBengkelDalam);
        return view('admin.perawatan-pemeliharaan.supervisor-check-armada.bengkel-dalam.list');
    }

    public function tambahBengkelDalam(Request $request)
    {
        $data = $request->all();
        $auth = Auth()->user()->name;
        BengkelDalam::create([
            'id_cek_armada' => $data['cek_armada'],
            'tgl_masuk' => now(),
            'svp_checker' => $auth,
            'status_bengkel' => 2
        ]);

        return redirect()->back();
    }

    public function checkBengkelDalam($id)
    {
        $item = BengkelDalam::findOrFail($id);
        $item->status_bengkel = 3;
        $item->save();

        return redirect()->back();
    }

    public function checkBengkelDalamSuccess($id)
    {
        $item = BengkelDalam::findOrFail($id);
        $item->tgl_keluar = now();
        $item->status_bengkel = 1;
        $item->save();

        return redirect()->back();
    }

}
