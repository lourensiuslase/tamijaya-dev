<?php

namespace App\Http\Controllers\Admin\PerawatanPemeliharaan\Sopir;

use App\Http\Controllers\Controller;
use App\Models\MasterData\Armada;
use App\Models\MasterData\Bahan_bakar;
use App\Models\MasterDataLogistik\Bagian;
use App\Models\PerawatanPemeliharaan\CekLayananFisik;
use App\Models\PerawatanPemeliharaan\PetugasCuci\CuciArmada;
use App\Models\PerawatanPemeliharaan\PetugasCek\CekArmada;
use App\Models\TataKelola\DokumenFinal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CekFisikLayananController extends Controller
{
    public function listCheckFisik()
    {
        $data = collect(DB::select(
            "SELECT bt.id
             , bs.booking_seats_seat_number as no_kursi
             , c.customer_name AS nama_penumpang
             , bt.booking_transactions_code AS kode_booking
             , bp.status AS status_pembelian
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_pick_point) AS pick_point
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_arrival_point) AS arrival_point
             ,pp.pick_point_eta AS jam
             ,sr.type_bus AS tipe
             ,a.armada_no_police AS nopol
             ,sr.driver_1
             ,sr.driver_2
             ,sr.conductor
             ,e.employee_name
             ,sr.destination

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             LEFT JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             LEFT JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             LEFT JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             LEFT JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             LEFT JOIN `armadas` a ON a.`id_pick_point` = pp.id
             LEFT JOIN `employees` e ON e.`id` = sr.driver_1
             WHERE bp.status LIKE '%PAID'
             "));
        $data2 = collect(DB::select(
            "SELECT bt.id
             , bs.booking_seats_seat_number as no_kursi
             , c.customer_name AS nama_penumpang
             , bt.booking_transactions_code AS kode_booking
             , bp.status AS status_pembelian
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_pick_point) AS pick_point
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_arrival_point) AS arrival_point
             ,pp.pick_point_eta AS jam
             ,sr.type_bus AS tipe
             ,a.armada_no_police AS nopol
             ,sr.driver_1
             ,sr.driver_2
             ,sr.conductor
             ,e.employee_name

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             LEFT JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             LEFT JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             LEFT JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             LEFT JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             LEFT JOIN `armadas` a ON a.`id_pick_point` = pp.id
             LEFT JOIN `employees` e ON e.`id` = sr.driver_2
             WHERE bp.status LIKE '%PAID'
             "));
        $data3 = collect(DB::select(
            "SELECT bt.id
             , bs.booking_seats_seat_number as no_kursi
             , c.customer_name AS nama_penumpang
             , bt.booking_transactions_code AS kode_booking
             , bp.status AS status_pembelian
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_pick_point) AS pick_point
             , (SELECT CONCAT(pp.`pick_point_origin`,' - ',pp.`pick_point_name`) FROM `pick_points` pp WHERE pp.id = bt.booking_transactions_arrival_point) AS arrival_point
             ,pp.pick_point_eta AS jam
             ,sr.type_bus AS tipe
             ,a.armada_no_police AS nopol
             ,sr.driver_1
             ,sr.driver_2
             ,sr.conductor
             ,e.employee_name

             FROM `booking_transactions` bt
             JOIN `customers` c ON c.id = bt.booking_transactions_customer_code
             LEFT JOIN `schedule_regulers` sr ON sr.`id` = bt.booking_transactions_schedule_id
             LEFT JOIN `booking_seats` bs ON sr.`id` = bs.booking_seats_schedule_id
             LEFT JOIN `booking_payments` bp ON bp.id_booking_transaction = bt.id
             LEFT JOIN `pick_points` pp ON pp.`id` = bt.booking_transactions_status
             LEFT JOIN `armadas` a ON a.`id_pick_point` = pp.id
             LEFT JOIN `employees` e ON e.`id` = sr.conductor
             WHERE bp.status LIKE '%PAID'
             "));
            //  dd($data);
        $armada = Armada::get();
        $bagian = Bagian::get();
        $bahan_bakar = Bahan_bakar::get();
        $sopir = CekLayananFisik::select("check_fisik_layanan.*", 'bagians.nama_bagian as bagian', 'armadas.armada_merk as merk','armadas.armada_no_police' , 'bahan_bakar.status')
            ->join('bagians', 'bagians.id', '=', 'check_fisik_layanan.bagian_id')
            ->join('armadas', 'armadas.id', '=', 'check_fisik_layanan.id_armada')
            ->join('bahan_bakar', 'bahan_bakar.id', '=', 'check_fisik_layanan.id_bbm')
            ->orderBy('created_at', 'desc')
            ->get();
        return view('admin.perawatan-pemeliharaan.sopir.check-fisik-layanan', compact('armada', 'bagian', 'sopir','data','data2','data3', 'bahan_bakar'));

//        return view('admin.perawatan-pemeliharaan.sopir.check-fisik-layanan', compact('armada', 'bagian', 'sopir'));
    }


    public function getArmada(Request $request)
    {
        $armadaId = $request->input('id_armada');

        $armada = Armada::with('pickPoint')->find($armadaId);

        if ($armada) {
            $data = [
                'pick_point_name' => $armada->pickPoint->pick_point_name,
                'destination_wisata_name' => $armada->destinationWisata->destination_wisata_name,
                'armada_category' => $armada->armada_category,
                'armada_type' => $armada->armada_type,
                'armada_capacity' => $armada->armada_capacity, // Tambahkan ini
                'armada_no_police' => $armada->armada_no_police,
            ];

            return response()->json($data);
        }

        return response()->json(['error' => 'Data not found'], 404);
    }


    public function TambahArmada(Request $request)
    {
        DB::beginTransaction();
        try {
            $validatedData = $request->validate([
                'id_armada' => 'required|numeric',
                'bagian_id' => 'required|numeric',
                'tgl_laporan' => 'required|date',
                'id_bbm' => 'required|numeric',
                // 'kilometer' => 'required|numeric',
                'keluhan' => 'required|string',
            ]);

            // Buat instance model CekLayananFisik
            $sopir = new CekLayananFisik();
            $sopir->id_armada = $validatedData['id_armada'];
            $sopir->bagian_id = $validatedData['bagian_id'];
            $sopir->tgl_laporan = $validatedData ['tgl_laporan'];
            $sopir->id_bbm = $validatedData ['id_bbm'];
            // $sopir->kilometer = $validatedData ['kilometer'];
            $sopir->keluhan = $validatedData['keluhan'];
            $sopir->status_cuci = NULL;
            $sopir->status_cek = NULL;

            $sopir->save();

            CuciArmada::create([
                'check_fisik_layanan_id' => $sopir->id
            ]);
            CekArmada::create([
                'check_fisik_layanan_id' => $sopir->id
            ]);
           
            
            DB::commit();
            session()->flash('message', ['Berhasil menyimpan data check fisik layanan', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message', ['Gagal menyimpan data check fisik layanan', 'error']);
        }

        return redirect()->route('perawatan-pemeliharaan.sopir.check-fisik-layanan');
    }


    public function AjukanCuciArmada (Request $request)
    {
        DB::beginTransaction();

        try {
            foreach ($request->check_fisik_layanan_id as $check_fisik_layanan_id) {
                $sopir = new CekArmada();
                $sopir->check_fisik_layanan_id = $check_fisik_layanan_id;

//                dd($sopir);
                $sopir->save();
            }

            DB::commit();
            Session::flash('message', ['Berhasil mengajukan pengajuan pembelian', 'success']);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', ['Gagal mengajukan pengajuan pembelian', 'error']);
        }

        return redirect()->route('perawatan-pemeliharaan.sopir.check-fisik-layanan');
    }

    public function ReportPerjalanan()
    {
        return view('admin.perawatan-pemeliharaan.sopir.report-perjalanan');
    }



}
