<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;

class BagianCek extends Model
{
    protected $table = 'bagian_cek';
    protected $fillable = [
        'name'
    ];
}
