<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;

class AgenPool extends Model
{
    protected $table = 'agen_pool';

    protected $guarded = ['id'];
}
