<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;

class BagianCuciArmada extends Model
{
    protected $table = 'bagian_cuci_armada';

    protected $fillable = [
        'id_armada', 
        'bagian_name', 
        'flag_eksterior', 
        'flag_interior'
    ];
}
