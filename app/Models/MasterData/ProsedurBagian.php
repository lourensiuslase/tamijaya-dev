<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;

class ProsedurBagian extends Model
{
    protected $table = 'prosedur_bagian'; 
    protected $fillable = [
        'id_bagian_cek', 
        'prosedur_bagian_name'
    ];
}
