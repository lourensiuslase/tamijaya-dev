<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;

class Pasengger extends Model
{
    protected $table = 'pasenggers';

    protected $fillable = ['customer_id', 'name', 'nik'];
}
