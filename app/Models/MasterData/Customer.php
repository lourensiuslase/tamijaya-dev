<?php

namespace App\Models\MasterData;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Customer extends Authenticatable implements AuthenticatableContract
{
    protected $table = 'customers';

    protected $fillable = ['customer_code', 'customer_name', 'id_city', 'id_province', 'customer_address', 'customer_phone', 'customer_email', 'customer_password', 'customer_nik', 'customer_address'];

    protected $hidden = [
        'customer_nik',
        'customer_code',
        'id_city',
        'id_province', 
        'remember_token'
    ];

    public function getAuthPassword()
    {
        return $this->customer_password;
    }
}
