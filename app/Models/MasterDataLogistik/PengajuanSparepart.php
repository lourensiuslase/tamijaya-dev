<?php

namespace App\Models\MasterDataLogistik;

use Illuminate\Database\Eloquent\Model;

class PengajuanSparepart extends Model
{
    protected $table = 'pengajuan_sparepart';

    protected $fillable = [
        'id_armada',
        'id_komponen',
        'pic_pemohon',
        'jml_permintaan',
        'deskripsi',
        'status_pengajuan_sparepart',
    ];
}
