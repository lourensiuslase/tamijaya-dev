<?php

namespace App\Models\PerawatanPemeliharaan\PetugasCek;

use Illuminate\Database\Eloquent\Model;

class CekArmada extends Model
{
    protected $table = 'cek_armada';
    protected $primaryKey = 'id';
    protected $fillable = [
        'check_fisik_layanan_id',
    ];

}

