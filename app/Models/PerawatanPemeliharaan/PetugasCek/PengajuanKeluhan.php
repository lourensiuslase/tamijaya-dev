<?php

namespace App\Models\PerawatanPemeliharaan\PetugasCek;

use Illuminate\Database\Eloquent\Model;

class PengajuanKeluhan extends Model
{
    protected $table = 'pengajuan_keluhan';

    protected $fillable = [
        'id_armada', 
        'id_prosedur_bagian', 
        'check_layanan_fisik', 
        'deskripsi'
    ];
}
