<?php

namespace App\Models\PerawatanPemeliharaan\Bengkel;

use Illuminate\Database\Eloquent\Model;

class BengkelDalam extends Model
{
    protected $table = 'bengkel_dalam';
    protected $fillable = [
        'id_cek_armada',
        'tgl_masuk',
        'tgl_keluar',
        'svp_checker',
        'status_bengkel',
    ];
}
