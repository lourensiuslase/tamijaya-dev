<?php

namespace App\Models\PerawatanPemeliharaan\Bengkel;

use Illuminate\Database\Eloquent\Model;

class FlagBengkelLuar extends Model
{
    protected $table = 'flag_bengkel_luar';

    protected $fillable = [
        'id_armada', 
        'nama',
        'flag_bengkel_luar',
        'tgl_ajukan', 
        'estimasi_harga',
        'estimasi_selesai', 
        'upload_pdf'
    ];
}
