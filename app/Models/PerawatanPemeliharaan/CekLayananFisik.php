<?php

namespace App\Models\PerawatanPemeliharaan;

use Illuminate\Database\Eloquent\Model;

class CekLayananFisik extends Model
{
    protected $table = 'check_fisik_layanan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_armada',
        'id_bbm',
        'bagian_id',
        'tgl_laporan',
        'status',
        'kilometer',
        'keluhan',
        'status_cuci',
        'status_kondisi',
        'status_cek',
        'spv_cuci',
        'spv_cek'
    ];

}
//        'id_pick_point',
//        'id_destination_wisata',
