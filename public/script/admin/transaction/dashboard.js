$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

let PickPointJogja = [];
let PickPointDenpasar = [];

function changeTujuan(e) {
    $("#detail_pick_point").trigger("change");
    $("#detail_arrival_point").trigger("change");

    if (e.value === "JOG-DPS") {
        var pickPointJogjaOptions = "";
        PickPointJogja.forEach(function(option) {
            pickPointJogjaOptions += "<option value='" + option.id + "'>" + option.text + "</option>";
        });
        $("#detail_pick_point").empty().append(pickPointJogjaOptions);
    
        var pickPointDenpasarOptions = "";
        PickPointDenpasar.forEach(function(option) {
            pickPointDenpasarOptions += "<option value='" + option.id + "'>" + option.text + "</option>";
        });
        $("#detail_arrival_point").empty().append(pickPointDenpasarOptions);
    
    } else if (e.value === "DPS-JOG") {
        var pickPointDenpasarOptions = "";
        PickPointDenpasar.forEach(function(option) {
            pickPointDenpasarOptions += "<option value='" + option.id + "'>" + option.text + "</option>";
        });
        $("#detail_pick_point").empty().append(pickPointDenpasarOptions);
    
        var pickPointJogjaOptions = "";
        PickPointJogja.forEach(function(option) {
            pickPointJogjaOptions += "<option value='" + option.id + "'>" + option.text + "</option>";
        });
        $("#detail_arrival_point").empty().append(pickPointJogjaOptions);
    
    }

}

$(document).ready(function () {

    // Get Destination
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "/booking-ticket/get/pick-point",
        delay: 800,
        success: function (data) {
            PickPointJogja = data.JOGJA;
            PickPointDenpasar = data.DENPASAR;

            $("#detail_pick_point", {
                data: PickPointJogja,
            });
            $("#detail_arrival_point", {
                data: PickPointDenpasar,
            });
        },
    });

});

// cek tiket
function cekTravel() {
    var tujuanTravel = $("#detail_tujuan").val();
    var dateTravel = $("#detail_date_departure").val();

    if (!tujuanTravel || !dateTravel) {
        alert("Please select the destination and departure date.");
        return;
    }

    $.ajax({
        type: "POST",
        url: "/booking-ticket/cek-schedule", 
        data: {
            detail_tujuan: tujuanTravel,
            detail_date_departure: dateTravel
        },
        success: function(response) {
            displaySchedule(response);
            selectSeat(response);
        },
        error: function(xhr, status, error) {
            $("#reguler-schedule-not-available").show();
            $("#schedule").hide();
        }
    });
}

function displaySchedule(scheduleData) {
    console.log("Received schedule data:", scheduleData);

    if (!Array.isArray(scheduleData.data)) {
        console.error("Error: No schedule data found.");
        return;
    }

    var scheduleTable = $("#schedule tbody");
    scheduleTable.empty();

    if (scheduleData.data.length === 0) {
        $("#reguler-schedule-not-available").show();
        $("#schedule").hide();
        return;
    }

    scheduleData.data.forEach(function(data){
        var armada_price = data.armada_type === 'SUITESS' ? '600.000' : '375.000';
        var row = "<tr>" +
            "<td>" + data.destination + "</td>" +
            "<td>" + data.armada_type + "</td>" +
            "<td>" + data.armada_no_police + "</td>" +
            "<td>" + armada_price + "</td>" +
            "<td>" + data.armada_seat + "</td>" +
            "<td><a href='/booking-ticket/detail/" + data.id + "' class='btn btn-danger' >Pilih</a></td>" +
            "</tr>";
    
        scheduleTable.append(row);
    });

    $("#reguler-schedule-not-available").hide();
    $("#schedule").show();
    
}

var submission_target = [];

function selectSeat(seat) {
    if (!submission_target.includes(seat)) {
        submission_target.push(seat);
    } else {
        var index = submission_target.indexOf(seat);
        submission_target.splice(index, 1);
    }

    var countPassenger = submission_target.length;
    var priceTicket = 375000;
    if ($("#travel_type_name").val() == "SUITESS") {
        priceTicket = 600000;
    }
    var totalPrice = countPassenger * parseFloat(priceTicket);
    $("#travel_detail_total_cost").val(totalPrice);

    var displayTotal = `Rp. ${numberWithCommas(totalPrice)}`
    $("#travel_selected_seat").val(submission_target.toString());
    $("#travel_passenger").val(countPassenger);
    $("#travel_total_price").val(displayTotal);

    var totalAmountTransfer = $("#travel_total_price").val();
    $(".title-transfer-amount").text(totalAmountTransfer);

    if (countPassenger > 1) {
        $("#customerPasengger").show();
        $("#customerPasengger1").show();
        $("#nikPasengger").show();
        $("#nikPasengger1").show();
    } else {
        $("#customerPasengger").hide();
        $("#customerPasengger1").hide();
        $("#nikPasengger").hide();
        $("#nikPasengger1").hide();
    }
    if (totalPrice > 0) {
        $(".submit_transaction_reguler").show();
    } else {
        $(".submit_transaction_reguler").hide();
    }

    // CALCULATE AGENT

    var checkAgent = $("#radio-agent").prop("checked")
    if (checkAgent) {
        var default_amount = totalPrice;
        var discount_agent = $("#travel_discount_agent").val();
        let val_discount = discount_agent.replace("Rp. ", "");
        val_discount = val_discount.replaceAll('.', '')
        var total_discount_agent = parseFloat(val_discount) * countPassenger;
        var total_price_agent = parseFloat(default_amount) - parseFloat(total_discount_agent);
        $("#travel_detail_total_cost").val(total_price_agent);
        $("#travel_detail_total_discount").val(total_discount_agent);
        $("#travel_price_agent").val(`Rp. ${numberWithCommas(total_price_agent)}`)

    }

}

const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

// payment tab

const choosePayment = () => {
    var checkCash = $("#payment_cash").prop("checked");
    var checkTransfer = $("#payment_transfer").prop("checked");
    if (checkCash) {
        $("#payment_method").val('CASH');
        $("#id_bank_transfer").removeClass("required");
        $("#row-bank-transfer").hide();
        $("#payment-description-cash").show();
        $("#payment-description-transfer").hide();
        return;
    } else if (checkTransfer) {
        $("#payment_method").val('TRANSFER');
        $("#id_bank_transfer").addClass("required");
        $("#id_bank_transfer").val("");
        $("#row-bank-transfer").show();
        $("#payment-description-cash").hide();
        return;
    }
};

const chooseBank = () => {
    var id_bank = $("#id_bank_transfer").val();
    $("#payment-description-transfer").hide();
    if (id_bank) {
        $("#id_payment").val(id_bank);
        $("#payment-description-transfer").show();
        var titleTransferBank = "";
        var accountTransferBank = "";
        var srcLogoBank = "";
        $.ajax({
            url: `/booking-ticket/booking/bank/${id_bank}`,
            method: "get",
            dataType: "json",
            success: function (response) {
                bank_name = response.data.bank_name;
                titleTransferBank = `${bank_name} Payment Transfer`;
                accountTransferBank = response.data.bank_account;

                if (bank_name == "BCA") {
                    srcLogoBank = "../../../images/logo bca.png";
                } else if (bank_name == "MANDIRI") {
                    srcLogoBank = "../../../images/logo mandiri.png";
                } else {
                    srcLogoBank = "../../../images/logo permata.png";
                }

                $("#title-transfer-payment").text(titleTransferBank);
                $("#title-transfer-account").text(accountTransferBank);
                $("#image-transfer-payment").attr("src", srcLogoBank);
            },
            error: function (err) {
                console.log(err);
            },
        });
    }
};

// image preview payment users
imgInp.onchange = evt => {
    const [file] = imgInp.files
        if (file) {
            blah.src = URL.createObjectURL(file)
        }
}